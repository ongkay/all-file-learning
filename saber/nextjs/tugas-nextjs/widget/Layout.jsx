import Navbar from '../components/Navbar';

export default function Layout({ children, home }) {
  return (
    <>
      <Navbar />
      {home && console.log('bukanhome')}
      {children}
    </>
  );
}
