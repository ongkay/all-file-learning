import Cookies from 'js-cookie';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

export default function Navbar() {
  let router = useRouter();
  const [token, setToken] = useState(null);

  useEffect(() => {
    if (Cookies.get('token') !== undefined) {
      if (token === null) {
        setToken(Cookies.get('token'));
      }
    }
  }, [token, setToken]);

  const handleLogout = async (e) => {
    Cookies.remove('token');
    router.push('/auth/login');
  };

  return (
    <>
      <div className="container flex items-center justify-between p-5 mx-auto md:px-10 ">
        <Link href="/">
          <h1 className="text-2xl font-bold">LOGO</h1>
        </Link>

        <div className="flex items-center justify-center gap-10 list-none">
          <li>
            <Link href={'/'}>Home</Link>
          </li>
          <li>
            <Link href={'/product'}>Product</Link>
          </li>

          {token ? (
            <li>
              <span onClick={handleLogout}>Logout</span>
            </li>
          ) : (
            <li>
              <Link href={'/auth/login'}>Login</Link>
            </li>
          )}
        </div>
      </div>
    </>
  );
}
