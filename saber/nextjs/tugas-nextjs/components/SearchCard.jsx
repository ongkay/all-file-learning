import Link from 'next/link';

export default function SearchCard() {
  const category = [
    'Shift',
    'Elektroini',
    'Game',
    'Hijab',
    'Sepatu',
    'Laptop',
    'Iphone',
    'Semangka',
  ];
  return (
    <section className="container flex flex-col justify-start gap-3 p-5 mx-auto lg:p-10 lg:flex-row ">
      <div className="flex min-w-fit grow ">
        <label htmlFor="search" className="flex w-full">
          <span>
            <svg
              className="w-10 h-10 p-2 text-white rounded-full bg-rose-500"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
              />
            </svg>
          </span>

          <div className="w-full ml-2">
            <input
              type="search"
              className="inline-block w-full px-4 py-2 text-gray-600 border-2 rounded-lg outline-none focus:bg-white focus:border-rose-300"
              placeholder="Search"
              aria-label="Search"
              aria-describedby="button-addon2"
              id="search"
            />
          </div>
        </label>
      </div>

      <div className="flex items-center max-w-full ">
        <ul className="flex flex-wrap gap-2 font-semibold ">
          {category.map((item) => {
            return (
              <li>
                <button className="inline-block px-2 text-sky-500 hover:text-sky-300 ">
                  <Link href="/#">{item}</Link>
                </button>
              </li>
            );
          })}
        </ul>
      </div>
    </section>
  );
}
