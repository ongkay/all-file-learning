import Image from 'next/image';

export default function HeroSection() {
  return (
    <section className="bg-rose-200">
      <div className="container flex flex-col-reverse items-center justify-between p-5 mx-auto lg:flex-row lg:p-10">
        <div className="flex flex-col gap-3 mt-10 lg:mt-0 basis-1/2">
          <h1 className="text-4xl font-bold tracking-tighter lg:text-5xl">
            Temukan produk pilihan kamu disini!!
          </h1>
          <p className="text-md">
            Nikmati diskon 10% setiap pembelian yang kamu lakukan
          </p>
          <div className="mt-5">
            <button className="inline-block px-5 py-2 text-white bg-red-500 hover:bg-red-400 rounded-2xl">
              Find Product
            </button>
          </div>
        </div>

        <div className="flex items-center justify-center grow basis-1/2">
          <div className="relative w-44 h-44">
            <Image src={'/img/shoping_hero.svg'} layout="fill" quality={80} />
          </div>
        </div>
      </div>
    </section>
  );
}
