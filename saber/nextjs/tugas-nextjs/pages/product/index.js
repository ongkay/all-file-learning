import Link from 'next/link';
import React, { useContext, useEffect, useState } from 'react';
import Layout from '../../widget/Layout';
import { GlobalContext } from '../../context/GlobalContext';
import fetchData from '../../services/api';

export async function getServerSideProps() {
  const res = await fetch('https://backendexample.sanbercloud.com/api/student-scores');
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export default function Product({ data }) {
  const [dataProducts, setDataProducts] = useState(data);

  const { state, handleFunction } = useContext(GlobalContext);
  let { input, fetchStatus, setFetchStatus } = state;
  let { handleChange, handleSubmit, handleDelete, handleEdit } = handleFunction;

  useEffect(() => {
    const getData = async () => {
      let { data } = await fetchData.get('/student-scores');

      setDataProducts([...data]);
    };

    if (fetchStatus) {
      getData();
      setFetchStatus(false);
    }
  }, [fetchStatus, setFetchStatus]);

  return (
    <>
      <Layout>
        <div className="container flex-col justify-start gap-2 p-10 mx-auto border-4">
          <div className="flex justify-start gap-2 my-5">
            {data.length !== 0 &&
              data.map((res) => {
                return (
                  <div key={res.id}>
                    <Link href={`/product/${res.id}`}>
                      <div className="inline-flex items-center p-2 text-sm leading-none bg-white rounded-full shadow text-teal">
                        <span className="inline-flex items-center justify-center h-6 px-3 text-white rounded-full bg-rose-600">
                          {res.course}
                        </span>
                        <span className="inline-flex px-2 text-gray-700">{res.name}</span>
                      </div>
                    </Link>
                  </div>
                );
              })}
          </div>

          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-white uppercase bg-rose-400 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" className="px-6 py-3">
                  Name
                </th>
                <th scope="col" className="px-6 py-3">
                  Course
                </th>
                <th scope="col" className="px-6 py-3">
                  Score
                </th>
                <th scope="col" className="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {dataProducts !== undefined &&
                dataProducts.map((res) => {
                  return (
                    <tr
                      key={res.id}
                      className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                    >
                      <th className="px-6 py-4">{res.name}</th>
                      <td className="px-6 py-4">{res.course}</td>
                      <td className="px-6 py-4">{res.score}</td>
                      <td className="px-6 py-4">
                        <button
                          value={res.id}
                          onClick={handleDelete}
                          className="font-medium text-rose-600 dark:text-rose-500 hover:underline"
                        >
                          Delete
                        </button>
                        <button
                          value={res.id}
                          onClick={handleEdit}
                          className="ml-2 font-medium text-rose-600 dark:text-rose-500 hover:underline"
                        >
                          Edit
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>

          <form onSubmit={handleSubmit} className="mt-12">
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Name
              </label>
              <input
                value={input.name}
                onChange={handleChange}
                name="name"
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-rose-500 focus:border-rose-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-rose-500 dark:focus:border-rose-500"
                placeholder="input name"
                required
              />
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                course
              </label>
              <input
                value={input.course}
                onChange={handleChange}
                name="course"
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-rose-500 focus:border-rose-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-rose-500 dark:focus:border-rose-500"
                placeholder="input course"
                required
              />
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                score
              </label>
              <input
                value={input.score}
                onChange={handleChange}
                name="score"
                type="text"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-rose-500 focus:border-rose-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-rose-500 dark:focus:border-rose-500"
                placeholder="input score"
                required
              />
            </div>

            <button
              type="submit"
              className="text-white bg-rose-700 hover:bg-rose-800 focus:ring-4 focus:outline-none focus:ring-rose-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-rose-600 dark:hover:bg-rose-700 dark:focus:ring-rose-800"
            >
              Submit
            </button>
          </form>
        </div>
      </Layout>
    </>
  );
}
