import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Layout from '../../widget/Layout'
import fetchData from '../../services/api'

export default function ProductId() {
  const router = useRouter()
  const { productId } = router.query
  const [productDetail, setProductDetail] = useState(false)

  useEffect(() => {
    if (productId !== undefined) {
      const getProductById = async () => {
        const { data } = await fetchData(
          `/student-scores/${router.query?.productId}`,
        )
        setProductDetail(data)
      }
      getProductById()
    }
  }, [productId])

  const handleProgress = (score) => {
    if (score === 100) return 'w-full'
    if (score >= 60 && score < 100) return 'w-11/12'
    if (score >= 50 && score < 60) return 'w-1/2'
    if (score >= 10 && score < 50) return 'w-1/4'
    if (score < 10) return 'w-0'
  }

  return (
    <>
      <Layout>
        {productDetail ? (
          <div className="container flex items-center justify-between p-5 mx-auto">
            <div className="relative w-full p-4 text-gray-700 bg-white shadow-lg rounded-xl dark:bg-gray-800 dark:text-gray-100">
              <a href="#" className="block w-full h-full">
                <div className="flex flex-col items-start justify-start w-full gap-5">
                  <p className="mb-4 text-2xl font-bold text-gray-700 dark:text-white">
                    {productDetail.name}
                  </p>
                  <div className="flex items-center justify-between w-full text-sm text-gray-400">
                    <p>
                      {productDetail.course} ( {productDetail.score} )
                    </p>
                    <p>{productDetail.score}</p>
                  </div>
                  <div className="w-full h-2 mb-4 rounded-full bg-rose-100">
                    <div
                      className={`${handleProgress(
                        parseInt(productDetail.score),
                      )} h-full text-xs text-center text-white
                    bg-rose-400 rounded-full`}
                    ></div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        ) : (
          <>Loading...</>
        )}
        <button
          onClick={() => {
            router.replace('/product')
          }}
        >
          Klik Me
        </button>
      </Layout>
    </>
  )
}
