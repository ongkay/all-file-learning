/* eslint-disable react/jsx-key */
export default function Peserta() {
  const datapeserta = {
    Nama: 'nama peserta',
    Email: 'email peserta',
    'Sistem Operasi yang digunakan': 'sistem operasi peserta',
    'Akun Gitlab': 'Akun gitlab peserta',
    'Akun Telegram': 'akun telegram',
    'Tanggal Lahir': '30-02-2022',
    'Pengalaman kerja': 'tulis jika belum ada',
  };

  return (
    <>
      <h1>Data Peserta Sanbercode Bootcamp Next JS</h1>
      <ol>
        {Object.entries(datapeserta).map(([key, val], i) => (
          <li key={i}>
            <b>{key}:</b> {val}
          </li>
        ))}
      </ol>
    </>
  );
}
