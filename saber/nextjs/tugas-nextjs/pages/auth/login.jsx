import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import fetchData from '../../services/api';

export default function Login() {
  let router = useRouter();

  const [input, setInput] = useState({
    email: '',
    password: '',
  });

  const handleChange = ({ target }) => {
    setInput({ ...input, [target.name]: target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const { data } = await fetchData.post('/user-login', {
      email: input.email,
      password: input.password,
    });

    console.log(data.token);

    router.push('/product');
    Cookies.set('token', data.token, { expires: 1 });
  };

  return (
    <>
      <form className="p-5" onSubmit={handleSubmit}>
        <label>Email</label>
        <br />
        <input
          className="inline-block h-5"
          onChange={handleChange}
          value={input.email}
          name="email"
          type="email"
        />
        <br />
        <label>Password</label>
        <br />
        <input
          className="inline-block h-5"
          onChange={handleChange}
          value={input.password}
          name="password"
          type="password"
        />
        <br />

        <br />
        <button className="px-2 text-white bg-gray-500" type="submit">
          Login
        </button>
      </form>
    </>
  );
}
