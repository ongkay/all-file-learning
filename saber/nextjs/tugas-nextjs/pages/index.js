import Layout from '../widget/Layout';
import ProductCard from '../components/ProductCard';
import SearchCard from '../components/SearchCard';
import HeroSection from '../components/HeroSection';

export default function Home() {
  return (
    <>
      <Layout home />
      <HeroSection />
      <SearchCard />
      <div className="container px-5 mx-auto lg:px-10">
        <h2 className="my-5 text-2xl font-bold">This Recommended for you!!!</h2>
        <ProductCard />
      </div>
    </>
  );
}
