import { createContext, useState } from 'react'
import fetchData from '../services/api'

const endpoints = '/student-scores'

export const GlobalContext = createContext()

export const GlobalProvider = (props) => {
  // state
  const [input, setInput] = useState({
    gender: '',
    name: '',
    height: '',
  })
  const [fetchStatus, setFetchStatus] = useState(false)
  const [currentId, setCurrentId] = useState(-1)

  // handleFunction
  const handleSubmit = async (e) => {
    e.preventDefault()

    const { name, course, score } = input

    if (currentId === -1) {
      await fetchData.post(endpoints, {
        name,
        course,
        score,
      })

      setFetchStatus(true)
    } else {
      await fetchData.put(`${endpoints}/${currentId}`, {
        name,
        course,
        score,
      })

      setFetchStatus(true)
    }

    setInput({
      name: '',
      course: '',
      score: '',
    })

    setCurrentId(-1)
  }

  const handleDelete = async ({ target }) => {
    await fetchData.delete(`${endpoints}/${target.value}`)
    setFetchStatus(true)
  }

  const handleEdit = async ({ target }) => {
    let { data } = await fetchData.get(`${endpoints}/${target.value}`)
    setCurrentId(data?.id)
    setInput({
      name: data?.name,
      course: data?.course,
      score: data?.score,
    })
  }

  const handleChange = ({ target }) => {
    setInput({ ...input, [target.name]: target.value })
    console.log(input)
  }

  let state = {
    input,
    setInput,
    fetchStatus,
    setFetchStatus,
    currentId,
    setCurrentId,
  }

  let handleFunction = {
    handleChange,
    handleSubmit,
    handleDelete,
    handleEdit,
  }

  return (
    <GlobalContext.Provider value={{ state, handleFunction }}>
      {props.children}
    </GlobalContext.Provider>
  )
}
