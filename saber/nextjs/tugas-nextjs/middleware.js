import { NextRequest, NextResponse } from 'next/server';

export function middleware(req, res) {
  if (req.nextUrl.pathname.startsWith('/product')) {
    if (req.cookies.get('token') === undefined) {
      return NextResponse.redirect(new URL('/auth/login', req.url));
    }
  }

  if (req.nextUrl.pathname.startsWith('/auth/login')) {
    if (req.cookies.get('token') !== undefined) {
      return NextResponse.redirect(new URL('/product', req.url));
    }
  }
}
