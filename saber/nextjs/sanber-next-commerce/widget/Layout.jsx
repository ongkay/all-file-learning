import React from 'react'
import Banner from '../components/Banner'
import Footer from '../components/Footer'
import Navbar from '../components/Navigation'

export default function Layout({ home, children }) {
  return (
    <div className="container mx-auto">
      <Navbar />
      {home && <Banner />}
      <div>{children}</div>
      <Footer />
    </div>
  )
}
