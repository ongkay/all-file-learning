import { Button, Spinner } from 'flowbite-react'
import { useRouter } from 'next/router'
import React, { useContext, useState } from 'react'
import { HiShoppingCart } from 'react-icons/hi'
import { GlobalContext } from '../context/GlobalContext'
import { getData, postData } from '../services/api'
import { ToastContainer, toast } from 'react-toastify'

import 'react-toastify/dist/ReactToastify.css'

export default function ButtonATC({ id, quantity, setQuantity, isHome }) {
  const router = useRouter()
  const { state, handleFunction } = useContext(GlobalContext)
  const {
    user,
    token,
    quantityCart,
    setActClick,
    setQuantityCart,
    setDataProducts,
  } = state
  const [loadingButton, setLoadingButton] = useState(false)
  const { getCheckOut } = handleFunction

  const quantityPlusHandler = () => setQuantity(quantity + 1)
  const quantityMinHandler = () => quantity > 1 && setQuantity(quantity - 1)

  const addToCartHeadler = async () => {
    if (user) {
      setLoadingButton(true)

      await postData(`/checkout/${user.id}/${id}`, { quantity }, token)

      if (isHome) {
        const getProduct = await getData('/product')
        setDataProducts(getProduct)
      }

      setQuantityCart(quantityCart + quantity)

      setLoadingButton(false)
      toast.success(`Add To Cart Success`, {
        position: 'top-center',
        autoClose: 2000,
      })

      setQuantity(1)
      getCheckOut()
      setActClick(true)
    } else {
      router.push('/auth/user-login')
    }
  }

  return (
    <>
      <div className="flex items-center justify-between mt-4 border">
        <button
          className="h-full px-2 text-black bg-gray-200"
          onClick={quantityMinHandler}
        >
          -
        </button>
        <input
          className="inline-block w-full h-full text-center focus:outline-none"
          value={quantity}
          onChange={() => {}}
        />
        <button
          className="h-full px-2 text-black bg-gray-200"
          onClick={quantityPlusHandler}
        >
          +
        </button>
      </div>
      <div className="mx-auto my-2">
        <Button
          size="lg"
          className="w-full rounded-none"
          onClick={addToCartHeadler}
        >
          {loadingButton ? (
            <>
              <div className="mr-3">
                <Spinner size="sm" light={true} />
              </div>
            </>
          ) : (
            <>
              <HiShoppingCart className="w-5 h-5 mr-2" />
              Add to Cart
            </>
          )}
        </Button>
        <ToastContainer />
      </div>
    </>
  )
}
