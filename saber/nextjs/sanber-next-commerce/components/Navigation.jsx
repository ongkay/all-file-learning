/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect } from 'react'
import { Navbar } from 'flowbite-react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Cookies from 'js-cookie'
import { HiOutlineCreditCard, HiOutlineShoppingCart } from 'react-icons/hi'
import { GlobalContext } from '../context/GlobalContext'
import SearchProducts from './SearchProducts'

export default function Navigation() {
  const router = useRouter()
  const { state, handleFunction } = useContext(GlobalContext)
  const { user, setUser, setToken, quantityCart } = state
  const { getCheckOut } = handleFunction

  useEffect(() => {
    if (Cookies.get('token_user') !== undefined) {
      if (!user) {
        setToken(Cookies.get('token_user'))
        setUser(JSON.parse(Cookies.get('user')))
      } else {
        if (!quantityCart) {
          getCheckOut()
        }
      }
    }
  }, [setToken, setUser, user])

  const userLogoutHandler = async (e) => {
    Cookies.remove('token_user')
    Cookies.remove('user')
    setUser(undefined)
    router.push('/auth/user-login')
  }

  return (
    <>
      <div className="sticky top-0 z-50">
        <Navbar fluid={false} rounded={true}>
          <div className="container flex flex-wrap items-center justify-between p-5 mx-auto">
            <Navbar.Toggle />

            <Navbar.Brand href="/">
              <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
                TokoKu
              </span>
            </Navbar.Brand>

            <div className="flex sm:hidden">
              <Link href={'/user/checkout'}>
                <div className="relative">
                  <HiOutlineShoppingCart size={25} className="text-gray-500 " />
                  <div className="absolute inline-flex items-center justify-center w-6 h-6 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-3 -right-3 dark:border-gray-900">
                    {quantityCart}
                  </div>
                </div>
              </Link>
            </div>

            <div className="hidden px-5 mx-auto grow lg:px-10 md:block">
              <SearchProducts />
            </div>

            <Navbar.Collapse>
              <ul className="flex flex-col p-3 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                <li className="w-full sm:hidden">
                  <SearchProducts />
                </li>
                <li className="hidden pr-6 border-gray-300 md:block">
                  <Link href={'/user/checkout'}>
                    <div className="relative">
                      <HiOutlineShoppingCart
                        size={25}
                        className="text-gray-500 "
                      />
                      <div className="absolute inline-flex items-center justify-center w-6 h-6 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-3 -right-3 dark:border-gray-900">
                        {quantityCart}
                      </div>
                    </div>
                  </Link>
                </li>

                {user && (
                  <li className="pr-6 border-gray-300">
                    <Link href={'/user/transaction'}>
                      <div className="relative">
                        <HiOutlineCreditCard
                          size={25}
                          className="text-gray-500 "
                        />
                      </div>
                    </Link>
                  </li>
                )}

                <li className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-gray-500 md:p-0 dark:text-gray-400 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">
                  {user ? (
                    <span onClick={userLogoutHandler}>Logout</span>
                  ) : (
                    <Link href={'/auth/user-login'}>Login</Link>
                  )}
                </li>
              </ul>
            </Navbar.Collapse>
          </div>
        </Navbar>
      </div>
    </>
  )
}
