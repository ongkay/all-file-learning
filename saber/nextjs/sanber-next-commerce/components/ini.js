import React, { useContext, useEffect, useState } from 'react'
import { Navbar } from 'flowbite-react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Cookies from 'js-cookie'
import axios from 'axios'
import { GlobalContext } from '../context/GlobalContext'

export default function Navigation() {
  const router = useRouter()
  let { state, handleFunction } = useContext(GlobalContext)
  let {
    token,
    setToken,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
    getCheckoutUser,
    setCheckoutUser,
  } = state
  let { fetchCheckoutUser } = handleFunction

  const [search, setSearch] = useState('')
  const [data, setData] = useState(null)

  const [displaySearch, setDisplaySearch] = useState(false)

  useEffect(() => {
    if (Cookies.get(`token_user`) !== undefined) {
      if (token === undefined) {
        setToken(JSON.parse(Cookies.get('user')))
      }
    }
    if (token !== undefined) {
      if (fetchCheckoutStatus) {
        fetchCheckoutUser()
        setFetchCheckoutStatus(false)
      }
    }
  }, [
    token,
    setToken,
    search,
    setSearch,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
    getCheckoutUser,
    setCheckoutUser,
    fetchCheckoutUser,
  ])

  const handleLogout = () => {
    Cookies.remove('token_user')
    Cookies.remove('user')
    setToken(null)
    router.push('/auth/user-login')
  }

  const handleSearch = (event) => {
    setDisplaySearch(true)
    setSearch(event.target.value)

    if (search !== '') {
      axios
        .get(`https://service-example.sanbercloud.com/api/product`)
        .then((res) => {
          // console.log(res.data);
          let data = res.data.filter((res) => {
            return res.available !== 0
          })

          let searchData = data.filter((res) => {
            return Object.values(res)
              .join('')
              .toLowerCase()
              .includes(search.toLowerCase())
          })

          setData(searchData)
        })
        .catch((err) => {
          alert(err)
        })
    }
  }
  return (
    <div className="sticky top-0 z-50">
      <Navbar fluid={false} rounded={true}>
        <div className="container flex flex-wrap items-center justify-between p-5 mx-auto">
          <Navbar.Brand href="/">
            <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
              TokoSiapa.com
            </span>
          </Navbar.Brand>
          <div className="flex sm:hidden">
            <Link
              href="/user/checkout"
              className="relative block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-300 md:bg-transparent md:text-gray-700 md:p-0 dark:text-white"
              aria-current="page"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                />
              </svg>
              <div className="absolute inline-flex items-center justify-center w-5 h-5 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-2 -right-0 dark:border-gray-900">
                {getCheckoutUser}
              </div>
            </Link>
            <Navbar.Toggle />
          </div>

          <div className="px-5 mx-auto grow lg:px-10">
            <form className="relative">
              <label
                htmlFor="default-search"
                className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
              >
                Search
              </label>
              <div className="relative">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5 text-gray-500 dark:text-gray-400"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </div>
                <input
                  onChange={handleSearch}
                  value={search}
                  type="search"
                  id="default-search"
                  className="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg md:mt-0 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Search Laptops, Phone..."
                  required
                />
              </div>
              {/* kondisional menampilkan hasil search */}
              {displaySearch && (
                <div className="absolute z-10 w-full px-6 bg-blue-300 ">
                  {/* icon X di hasil search */}
                  <div className="pt-2">
                    <button
                      onClick={() => {
                        setDisplaySearch(false), setSearch('')
                      }}
                    >
                      <svg
                        className="w-6 h-6"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M6 18L18 6M6 6l12 12"
                        ></path>
                      </svg>
                    </button>
                  </div>
                  {data !== null &&
                    data.map((res) => {
                      return (
                        <span
                          onClick={() =>
                            router.push(`/detail-product/${res.id}`)
                          }
                          key={res.id}
                          className="block py-2 border-b-2 cursor-pointer"
                        >
                          {res.product_name}
                        </span>
                      )
                    })}
                </div>
              )}
            </form>
          </div>
          <Navbar.Collapse>
            <ul className="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
              <li className="pr-6 border-r-2 border-gray-300">
                <Link
                  href="/user/checkout"
                  className="relative hidden py-2 pl-3 pr-4 text-gray-700 rounded md:block hover:bg-gray-300 md:bg-transparent md:text-gray-700 md:p-0 dark:text-white"
                  aria-current="page"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                    />
                  </svg>
                  <div className="absolute inline-flex items-center justify-center w-5 h-5 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-2 -right-2 dark:border-gray-900">
                    {getCheckoutUser}
                  </div>
                </Link>
              </li>
              {/* transaction section */}
              {token && (
                <li className="pr-6 border-r-2 border-gray-300">
                  <Link
                    href="/user/transaction"
                    className="relative block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-300 md:bg-transparent md:text-gray-700 md:p-0 dark:text-white"
                    aria-current="page"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z"
                      />
                    </svg>
                  </Link>
                </li>
              )}

              <li className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-gray-500 md:p-0 dark:text-gray-400 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">
                {token ? (
                  <span onClick={handleLogout}>
                    <Link href="">Logout</Link>
                  </span>
                ) : (
                  <Link href={'/auth/user-login'}>Login</Link>
                )}
              </li>
            </ul>
          </Navbar.Collapse>
        </div>
      </Navbar>
    </div>
  )
}
