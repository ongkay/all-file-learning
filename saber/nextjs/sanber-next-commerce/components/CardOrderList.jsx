import React from 'react'
import formatRp from '../utils/formatRp'

export default function CardOrderList({ data }) {
  return (
    <div className="flex justify-between my-2 overflow-hidden border-b border-gray-100">
      <div className="w-3/4 grow">
        <h5 className="font-medium text-gray-700 truncate">
          {data.product?.product_name}
        </h5>
        <p className="text-sm text-gray-600"> {data.quantity} unit</p>
      </div>
      <div className="w-1/4 mb-4">
        <p className="mt-2 font-medium text-right text-gray-700 ">
          Rp {formatRp(data.unit_price)}
        </p>
      </div>
    </div>
  )
}
