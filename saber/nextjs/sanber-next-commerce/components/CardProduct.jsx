/* eslint-disable react-hooks/exhaustive-deps */
import { Button } from 'flowbite-react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import React, { useContext, useEffect, useState } from 'react'
import { GlobalContext } from '../context/GlobalContext'
import formatRp from '../utils/formatRp'

import 'react-toastify/dist/ReactToastify.css'
import ButtonATC from './ButtonATC'

export default function CardProduct({ data, isHome = true }) {
  const router = useRouter()
  const [quantity, setQuantity] = useState(1)

  return (
    <>
      <div className="border-2 border-gray-100 max-w-[300px] rounded-xl shadow-md overflow-hidden">
        <div className="bg-slate-300">
          <Image
            src={data.image_url}
            alt={data.product_name}
            quality={80}
            width={250}
            height={250}
            className="object-cover w-full h-56"
          />
        </div>
        <div className="p-6">
          <small>
            <span className="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-r-lg dark:bg-green-200 dark:text-green-900">
              {data.category.category_name}
            </span>
          </small>
          <h5 className="mt-4 truncate">{data.product_name}</h5>
          <ul className="mt-5 text-sm font-thin text-gray-500 ">
            <li>Stock : {data.stock}</li>
            <li className="text-lg font-bold">
              Harga : Rp {formatRp(data.price)}
            </li>
          </ul>

          <ButtonATC
            id={data.id}
            setQuantity={setQuantity}
            quantity={quantity}
            isHome={isHome}
          />

          <div className="mx-auto my-3">
            <Button
              size="md"
              color="gray"
              className="w-full rounded-none"
              onClick={() => {
                router.push(`/detail-product/${data.id}`)
              }}
            >
              Detail Product
            </Button>
          </div>
        </div>
      </div>
    </>
  )
}
