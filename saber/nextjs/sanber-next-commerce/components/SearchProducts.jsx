import { TextInput } from 'flowbite-react'
import React, { useContext, useState } from 'react'
import { HiSearch, HiX } from 'react-icons/hi'
import { GlobalContext } from '../context/GlobalContext'

export default function SearchProducts() {
  const { state } = useContext(GlobalContext)
  let { dataProducts } = state

  const [displaySearch, setDisplaySearch] = useState(false)
  const [inputSearch, setInputSearch] = useState('')
  const [dataSearch, setDataSearch] = useState(null)

  const hideSearchHandler = () => {
    setDisplaySearch(false)
    setInputSearch('')
  }

  const inputSearchHandler = ({ target }) => {
    setDisplaySearch(true)
    setInputSearch(target.value.toLowerCase())

    if (inputSearch !== '' && dataProducts) {
      let filteredData = dataProducts.filter((res) => {
        return res.available !== 0
      })

      filteredData = filteredData.filter((res) => {
        if (inputSearch !== '') {
          return res.product_name.toLowerCase().includes(inputSearch)
        }
      })
      setDataSearch(filteredData)
    }
  }

  return (
    <>
      <form className="relative">
        <TextInput
          id="search1"
          type="search"
          placeholder="Search Laptops, Phone..."
          icon={HiSearch}
          onChange={inputSearchHandler}
        />
        {displaySearch && (
          <div className="absolute z-10 w-full px-5 bg-white border">
            <div className="py-6">
              <button onClick={hideSearchHandler}>
                <HiX size={24} className="text-gray-500 " />
              </button>
              {dataSearch &&
                dataSearch.map((res) => {
                  return (
                    <>
                      <span
                        key={res.id}
                        className="block py-2 border-b border-gray-200 cursor-pointer"
                      >
                        {res.product_name}
                      </span>
                    </>
                  )
                })}
            </div>
          </div>
        )}
      </form>
    </>
  )
}
