import { createContext, useState } from 'react'
import { getData } from '../services/api'

export const GlobalContext = createContext()

export const GlobalProvider = (props) => {
  // state
  const [dataProducts, setDataProducts] = useState(undefined)
  const [user, setUser] = useState(undefined)
  const [token, setToken] = useState(undefined)
  const [quantityCart, setQuantityCart] = useState(0)
  const [dataCart, setDataCart] = useState(0)
  const [totalCart, setTotalCart] = useState(0)
  const [actClick, setActClick] = useState(false)

  // handleFunction
  const getCheckOut = async () => {
    const getProduct = await getData(`/checkout-product-user/${user.id}`, token)

    const filterData = getProduct.filter((res) => {
      return res.is_transaction == 0
    })
    setDataCart(filterData)

    let totalQuantity = 0
    let totalPrice = 0
    filterData.map((item) => {
      totalQuantity += parseInt(item.quantity)
      totalPrice += parseInt(item.unit_price)
    })
    setQuantityCart(totalQuantity)
    setTotalCart(totalPrice)
  }

  let state = {
    dataProducts,
    setDataProducts,
    user,
    setUser,
    token,
    setToken,
    dataCart,
    setDataCart,
    quantityCart,
    setQuantityCart,
    totalCart,
    setTotalCart,
    actClick,
    setActClick,
  }

  let handleFunction = {
    getCheckOut,
  }

  return (
    <GlobalContext.Provider value={{ state, handleFunction }}>
      {props.children}
    </GlobalContext.Provider>
  )
}
