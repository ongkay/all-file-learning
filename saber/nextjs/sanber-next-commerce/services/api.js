import axios from 'axios'
import { toast } from 'react-toastify'

const instance = axios.create({
  baseURL: 'https://service-example.sanbercloud.com/api',
})

//error Handler
instance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error) {
      if (error.response) {
        let message = error.response.data

        if (typeof message === 'string') toast.error(message)

        // return Promise.reject(error)
      } else {
        console.log(error)
        alert('axios error')
      }
    }
  },
)

export function getData(url, token) {
  return instance.get(`${url}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}

export async function postData(url, payload, token) {
  return await instance.post(`${url}`, payload, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}
export async function putData(url, payload, token) {
  return await instance.put(`${url}`, payload, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}

export async function deleteData(url, token) {
  return await instance.delete(`${url}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}
