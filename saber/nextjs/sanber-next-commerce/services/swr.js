import useSWR from 'swr'

const urlAPI = 'https://service-example.sanbercloud.com/api'
const fetcher = (url) => fetch(`${urlAPI}${url}`).then((res) => res.json())

export default function useFetch(endPoint) {
  const { data, error } = useSWR(endPoint, fetcher)

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  }
}
