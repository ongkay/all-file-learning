import { Button, Checkbox, Label, TextInput } from 'flowbite-react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { HiKey, HiMail } from 'react-icons/hi'
import { postData } from '../../services/api'
import jwtdecode from 'jwt-decode'
import Cookies from 'js-cookie'

export default function Login() {
  const router = useRouter()

  const [input, setInput] = useState({
    email: '',
    password: '',
  })

  const handleChange = ({ target }) => {
    setInput({ ...input, [target.name]: target.value })
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    const getProduct = await postData('/login', input)
    const cekLogin = jwtdecode(getProduct.token)

    if (cekLogin.role !== 'admin') {
      Cookies.set('token_user', getProduct.token, { expires: 1 })
      Cookies.set('user', JSON.stringify(getProduct.user), { expires: 1 })

      router.push('/')
    }
  }

  return (
    <>
      <div className="flex items-center justify-center w-full h-screen">
        <div class="flex flex-col w-full max-w-md px-4 py-8 bg-slate-50 rounded-lg shadow-lg dark:bg-gray-800 sm:px-6 md:px-8 lg:px-10">
          <div class="self-center mb-6 text-xl font-light text-gray-600 sm:text-2xl dark:text-white">
            LOGIN ACCOUNT
          </div>
          <form onSubmit={handleSubmit} className="flex flex-col gap-4">
            <TextInput
              id="email1"
              type="email"
              placeholder="Emailmu"
              required={true}
              icon={HiMail}
              name="email"
              onChange={handleChange}
              value={input.email}
            />
            <TextInput
              id="password1"
              type="password"
              placeholder="Paswordmu"
              required={true}
              icon={HiKey}
              name="password"
              onChange={handleChange}
              value={input.password}
            />

            <div className="flex items-center gap-2">
              <Checkbox id="remember" />
              <Label htmlFor="remember">Remember me</Label>
            </div>
            <Button type="submit">Login Now</Button>
          </form>

          <div clasname="flex items-center justify-center mt-6">
            <Link
              href="/auth/user-register"
              // target="_blank"
              clasname="inline-flex items-center text-base font-light text-center text-gray-500 hover:text-blue-800 dark:text-gray-100 dark:hover:text-white"
            >
              <span clasname="ml-2">Belom punya akun ? Daftar sekarang!</span>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
