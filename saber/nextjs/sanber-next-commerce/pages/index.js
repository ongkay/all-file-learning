/* eslint-disable react-hooks/exhaustive-deps */
import Link from 'next/link'
import { useContext, useEffect, useState } from 'react'
import CardProduct from '../components/CardProduct'
import Category from '../components/Category'
import { getData } from '../services/api'
import Layout from '../widget/Layout'
import { HiChevronDown } from 'react-icons/hi'
import { Button, Spinner } from 'flowbite-react'
import { GlobalContext } from '../context/GlobalContext'

export async function getServerSideProps() {
  const products = await getData('/product')

  return {
    props: {
      products,
    },
  }
}

export default function Home({ products }) {
  const { state } = useContext(GlobalContext)
  let { dataProducts, setDataProducts } = state

  const [limit, setLimit] = useState(4)
  const [loadingLoadMore, setLoadingLoadMore] = useState(false)
  const [randomProduct, setRandomProducts] = useState(null)

  useEffect(() => {
    if (dataProducts) {
      let filter = dataProducts.filter((res) => {
        return res.available === 1
      })
      let random = Math.floor(Math.random() * filter.length)
      setRandomProducts(random)
    } else {
      setDataProducts(products)
    }
  }, [dataProducts])

  const handleCounterFilter = () => {
    setLoadingLoadMore(true)

    setTimeout(() => {
      setLimit(limit + 4)
      setLoadingLoadMore(false)
    }, 500)
  }

  return (
    <>
      <Layout home>
        {dataProducts && (
          <>
            <div className="my-10">
              <h1 className="mt-3 text-3xl font-bold">Produk Rekomendasi</h1>
            </div>
            <div className="flex flex-wrap items-center justify-center gap-10 lg:justify-start">
              {products
                .filter((res, index) => {
                  return (
                    res.available === 1 &&
                    index > randomProduct &&
                    index < randomProduct + 3
                  )
                })
                .map((res) => {
                  return <CardProduct key={res.id} data={res} />
                })}
            </div>
            <div className="px-3 pt-10 my-10">
              <span className="font-bold text-blue-600">
                <Link href={`#`}>Lihat Semua Produk</Link>{' '}
              </span>
              <h1 className="mt-3 text-3xl font-bold">Produk Pilihan</h1>
            </div>
            <div className="px-3 mb-5">
              <Category />
            </div>
            <div className="flex flex-wrap items-center justify-center gap-10 lg:justify-start">
              {dataProducts
                .filter((item) => {
                  return item.available === 1
                })
                .slice(0, limit)
                .map((res) => {
                  return <CardProduct key={res.id} data={res} />
                })}
            </div>

            {!loadingLoadMore ? (
              <>
                <div className="container flex items-center justify-center mt-10">
                  <Button outline={true} onClick={handleCounterFilter}>
                    View More
                    <HiChevronDown className="w-5 h-5 ml-2" />
                  </Button>
                </div>
              </>
            ) : (
              <>
                <div className="container flex items-center justify-center mt-10">
                  <Button>
                    <div className="mr-3">
                      <Spinner size="sm" light={true} />
                    </div>
                    Loading ...
                  </Button>
                </div>
              </>
            )}
          </>
        )}
      </Layout>
    </>
  )
}
