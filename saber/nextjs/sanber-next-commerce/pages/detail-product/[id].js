/* eslint-disable react-hooks/exhaustive-deps */
import Image from 'next/image'
import { useRouter } from 'next/router'
import React, { useContext, useEffect, useState } from 'react'
import ButtonATC from '../../components/ButtonATC'
import { GlobalContext } from '../../context/GlobalContext'
import useFetch from '../../services/swr'
import formatRp from '../../utils/formatRp'
import Layout from '../../widget/Layout'

export default function DetailData() {
  const { id } = useRouter().query
  const { data, isLoading } = useFetch(`/product/${id}`)
  const { state } = useContext(GlobalContext)
  const { quantityCart } = state
  const [stockProduct, setStockProduct] = useState(null)
  const [quantity, setQuantity] = useState(1)

  useEffect(() => {
    stockProduct >= 1 && setStockProduct(stockProduct - quantity)
  }, [quantityCart])

  if (data && !stockProduct) {
    setStockProduct(data.stock)
  }

  if (isLoading) return <>Loading...</>

  return (
    <Layout>
      {data && (
        <div className="container p-5">
          <h1 className="py-5 text-2xl font-bold">Detail Product</h1>

          <div className="mt-10 ">
            <div className="flex-wrap w-full overflow-hidden bg-white border rounded-lg shadow-lg sm:flex-none lg:flex">
              <div className="object-cover ">
                <Image
                  src={data.image_url}
                  alt="produk"
                  objectFit="cover"
                  quality={80}
                  width={384}
                  height={256}
                />
              </div>
              <div className="w-full p-4 lg:w-2/3 grow">
                <h1 className="text-2xl font-bold text-gray-900">
                  {data.product_name}
                </h1>
                <p className="mt-2 text-sm text-gray-600">{data.description}</p>
                <div className="flex mt-2 item-center">
                  <p className="text-sm">Stock : {stockProduct}</p>
                </div>
                <div className="flex justify-between mt-3 item-center">
                  <h1 className="text-xl font-bold text-gray-700">
                    Rp {formatRp(data.price)}
                  </h1>
                </div>
                <div className="w-full lg:w-1/4">
                  <ButtonATC
                    id={id}
                    setQuantity={setQuantity}
                    quantity={quantity}
                    isHome={false}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </Layout>
  )
}
