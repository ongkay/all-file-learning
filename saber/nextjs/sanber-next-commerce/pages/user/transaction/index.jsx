/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react'
import { GlobalContext } from '../../../context/GlobalContext'
import Layout from '../../../widget/Layout'
import formatRp from '../../../utils/formatRp'
import { deleteData, getData, postData } from '../../../services/api'
import { Button, Spinner } from 'flowbite-react'
import { HiCheckCircle, HiOutlineTrash } from 'react-icons/hi'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

export default function Transaction() {
  const { state } = useContext(GlobalContext)
  const { token, user } = state
  const [data, setData] = useState(null)
  const [satusFetch, setStatusFetch] = useState(true)

  const transactionDoneHandler = (idTransaksi) => {
    NProgress.start()

    postData(`/transaction-completed/${idTransaksi}/${user.id}`, {}, token)
      .then(() => {
        setStatusFetch(true)
      })
      .catch((err) => {
        alert(err)
      })
  }

  const deleteTransactionHandler = (idTransaksi) => {
    NProgress.start()

    deleteData(`/transaction/${idTransaksi}`, token)
      .then(() => {
        setStatusFetch(true)
      })
      .catch((err) => {
        alert(err)
      })
  }

  useEffect(() => {
    if (user) {
      if (satusFetch) {
        getData(`/transaction-user/${user.id}`, token).then((res) => {
          setData(res.data)
        })

        setStatusFetch(false)
        NProgress.done()
      }
    }
  }, [satusFetch, setStatusFetch, user])

  return (
    <Layout>
      <div className="py-10">
        <div className="p-5 mx-auto">
          <h1 className="text-2xl font-bold">Transaction </h1>
          <p className="py-2 text-lg font-bold text-gray-400">
            Berikut daftar transaksi anda :
          </p>
        </div>

        {data &&
          data
            .filter((res) => {
              return res.status === 'Transaksi terdaftar'
            })
            .map((res) => {
              return (
                <div
                  key={res.id}
                  className="items-center w-full p-5 rounded-md shadow-md lg:flex lg:justify-between "
                >
                  <div>
                    <div className="">
                      <p className="mr-5 font-semibold truncate text-rose-600 lg:mr-5">
                        {res.status}
                      </p>
                      <p className="py-2 text-xs">
                        Code Transaksi : {res.transaction_code}
                      </p>
                      <hr />
                    </div>
                    <div className="text-sm font-semibold">
                      - Nama : {res.user.name}
                    </div>
                    <div className="text-sm font-semibold">
                      - Bank : {res.bank.bank_name} - {res.bank.virtual_account}
                    </div>
                    <div className="text-sm font-semibold">
                      - Total: Rp. {formatRp(res.total)}
                    </div>
                  </div>
                  <div className="flex justify-center mx-auto mt-3 lg:flex-none lg:justify-end lg:mx-0">
                    <div>
                      <Button onClick={() => transactionDoneHandler(res.id)}>
                        <HiCheckCircle className="w-5 h-5 mr-2" />
                        Selesaikan Transaksi
                      </Button>
                    </div>
                  </div>
                </div>
              )
            })}

        <p className="p-5 mt-10 text-lg font-bold text-gray-400">
          Transaksi Sudah Selesai :
        </p>

        {data &&
          data
            .filter((res) => {
              return res.status === 'Transaksi selesai'
            })
            .map((res) => {
              return (
                <div
                  key={res.id}
                  className="items-center w-full p-5 rounded-md shadow-md lg:flex lg:justify-between "
                >
                  <div>
                    <div className="">
                      <p className="mr-5 font-semibold text-green-600 truncate lg:mr-5">
                        {res.status}
                      </p>
                      <p className="py-2 text-xs">
                        Code Transaksi : {res.transaction_code}
                      </p>
                      <hr />
                    </div>
                    <div className="text-sm font-semibold">
                      - Nama : {res.user.name}
                    </div>
                    <div className="text-sm font-semibold">
                      - Bank : {res.bank.bank_name} - {res.bank.virtual_account}
                    </div>
                    <div className="text-sm font-semibold">
                      - Total: Rp. {formatRp(res.total)}
                    </div>
                  </div>
                  <div className="flex justify-center mx-auto mt-3 lg:flex-none lg:justify-end lg:mx-0">
                    <Button
                      onClick={() => deleteTransactionHandler(res.id)}
                      color="failure"
                    >
                      <HiOutlineTrash className="w-5 h-5 mr-2" />
                      Hapus
                    </Button>
                  </div>
                </div>
              )
            })}
      </div>
    </Layout>
  )
}
