import { Button, Label, Select, Spinner, TextInput } from 'flowbite-react'
import React, { useContext, useRef, useState } from 'react'
import { HiMail, HiShoppingCart, HiUser } from 'react-icons/hi'
import CardOrderList from '../../../components/CardOrderList'
import { GlobalContext } from '../../../context/GlobalContext'
import formatRp from '../../../utils/formatRp'
import Layout from '../../../widget/Layout'
import emailjs from '@emailjs/browser'
import useFetch from '../../../services/swr'
import { postData } from '../../../services/api'

export default function Checkout() {
  const { data: dataBank } = useFetch(`/bank`)
  const form = useRef()
  const { state } = useContext(GlobalContext)
  const { user, token, dataCart, totalCart } = state
  const [optionBankId, setOptionBankId] = useState(-1)
  const [loadingButton, setLoadingButton] = useState(false)

  let sendMail = null
  if (dataCart) {
    sendMail = `${dataCart.map((data) => {
      return `<li> x${data.quantity} - ${
        data.product?.product_name
      }  = Rp ${formatRp(data.unit_price)}</li>`
    })}
    Total Transfer Sebesar Rp ${formatRp(totalCart)}`
  }

  const transactionSubmitHandler = async (e) => {
    e.preventDefault()
    try {
      setLoadingButton(true)
      await postData(
        `/transaction/${user.id}`,
        { id_bank: optionBankId },
        token,
      )

      await emailjs.sendForm(
        'service_gmail',
        'template_x87u2s9',
        form.current,
        'laWA6dryIJ178iNvW',
      )
      setLoadingButton(false)
      window.location = '/user/transaction'
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <Layout>
      <div className="container px-5 my-10">
        <div className="col-span-4 p-4 mx-auto border border-gray-200 rounded-lg shadow-xl lg:w-1/2">
          {!dataCart ? (
            <>Loading...</>
          ) : (
            <>
              <h4 className="mb-4 text-xl font-medium text-gray-800 uppercase">
                order summary
              </h4>
              <div>
                {dataCart.map((data, index) => {
                  return (
                    <div key={index}>
                      <CardOrderList data={data} />
                    </div>
                  )
                })}
              </div>

              <div className="flex justify-between py-3 text-xl font-medium text-gray-800 uppercas">
                <p className="font-semibold">Total</p>
                <p className="font-semibold">Rp {formatRp(totalCart)}</p>
              </div>

              <div className="my-3 space-x-8">
                <form
                  ref={form}
                  onSubmit={transactionSubmitHandler}
                  className="flex flex-col gap-4"
                >
                  <div id="select">
                    <div className="block mb-2">
                      <Label
                        htmlFor="banktransf"
                        value="Select Bank Transfer"
                      />
                    </div>
                    <Select
                      id="banktransf"
                      name="name"
                      required={true}
                      placeholder="Pilih bank"
                      onChange={({ target }) => {
                        setOptionBankId(target.value)
                      }}
                      value={optionBankId}
                    >
                      {dataBank?.map((bank) => {
                        return (
                          <>
                            <option key={bank.id} value={bank.id}>
                              {bank?.bank_name} - {bank?.virtual_account}
                            </option>
                          </>
                        )
                      })}
                    </Select>
                  </div>

                  <TextInput
                    id="name1"
                    type="text"
                    placeholder="Namamu"
                    required={true}
                    icon={HiUser}
                    name="user_name"
                  />
                  <TextInput
                    id="email1"
                    type="email"
                    placeholder="Emailmu"
                    required={true}
                    icon={HiMail}
                    name="user_email"
                  />

                  <textarea
                    name="my_html"
                    value={sendMail}
                    className="hidden"
                  />

                  <div className="my-3">
                    <Button
                      size="lg"
                      className="w-full "
                      type="submit"
                      // onClick={addToCartHeadler}
                    >
                      {loadingButton ? (
                        <>
                          <div className="mr-3">
                            <Spinner size="sm" light={true} />
                          </div>
                        </>
                      ) : (
                        <>
                          <HiShoppingCart className="w-5 h-5 mr-2" />
                          Place order
                        </>
                      )}
                    </Button>
                  </div>
                </form>
              </div>
            </>
          )}
        </div>
      </div>
    </Layout>
  )
}
