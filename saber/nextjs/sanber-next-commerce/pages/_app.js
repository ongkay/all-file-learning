import Head from 'next/head'
import { GlobalProvider } from '../context/GlobalContext'
import '../styles/globals.css'
import { Router } from 'next/router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

NProgress.configure({ showSpinner: false })
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Tokoku | Jual beli online</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="ini adalah projek ecommerce percobaan"
        />
        <meta
          name="keywords"
          content="buat ecommerce, jual beli, mantul ecommerce, tokoku mantap"
        />
        <meta name="author" content="saya sendiri" />
      </Head>
      <GlobalProvider>
        <Component {...pageProps} />
      </GlobalProvider>
    </>
  )
}

export default MyApp
