import { Carousel } from 'flowbite-react'
import Image from 'next/image'
import React from 'react'

export default function Banner() {
  return (
    <>
      <div className="container h-56 mx-auto sm:h-64 xl:h-96 2xl:h96">
        <Carousel>
          <div className="relative h-32 lg:h-full">
            <Image src="/images/banner1.png" alt="banner 1" layout="fill" />
          </div>
          <div className="relative h-32 lg:h-full">
            <Image src="/images/banner2.png" alt="banner 2" layout="fill" />
          </div>
        </Carousel>
      </div>
    </>
  )
}
