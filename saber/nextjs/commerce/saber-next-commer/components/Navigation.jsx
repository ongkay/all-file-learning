import React, { useEffect, useState } from 'react'
import { Navbar } from 'flowbite-react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Cookies from 'js-cookie'

export default function Navigation() {
  const router = useRouter()
  const [token, setToken] = useState(null)

  useEffect(() => {
    if (Cookies.get('token_user') !== undefined) {
      if (token === null) {
        setToken(JSON.parse(Cookies.get('user')))
      }
    }
  }, [token, setToken])

  const handleLogout = async (e) => {
    Cookies.remove('token_user')
    Cookies.remove('user')
    setToken(null)
    router.push('/auth/user-login')
    // window.location = '/auth/user-login'
  }

  return (
    <>
      <Navbar fluid={false} rounded={true}>
        <div className="container flex flex-wrap items-center justify-between p-5 mx-auto">
          <Navbar.Brand href="/">
            <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
              TokoKu
            </span>
          </Navbar.Brand>
          <div className="flex sm:hidden">
            <a
              href="#"
              className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-300 md:bg-transparent md:text-gray-700 md:p-0 dark:text-white"
              aria-current="page"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                />
              </svg>
            </a>
            <Navbar.Toggle />
          </div>

          <div className="px-5 mx-auto grow lg:px-10">
            <form>
              <label
                htmlFor="default-search"
                className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
              >
                Search
              </label>
              <div className="relative">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5 text-gray-500 dark:text-gray-400"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </div>
                <input
                  type="search"
                  id="default-search"
                  className="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg md:mt-0 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Search Laptops, Phone..."
                  required
                />
              </div>
            </form>
          </div>
          <Navbar.Collapse>
            <ul className="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
              <li className="pr-6 border-r-2 border-gray-300">
                <a
                  href="#"
                  className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-300 md:bg-transparent md:text-gray-700 md:p-0 dark:text-white"
                  aria-current="page"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    class="w-6 h-6"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                    />
                  </svg>
                </a>
              </li>

              <li className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-gray-500 md:p-0 dark:text-gray-400 md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">
                {token ? (
                  <span onClick={handleLogout}>Logout</span>
                ) : (
                  <Link href={'/auth/user-login'}>Login</Link>
                )}
              </li>
            </ul>
          </Navbar.Collapse>
        </div>
      </Navbar>
    </>
  )
}
