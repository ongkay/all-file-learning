import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import formatRp from '../utils/formatRp'

export default function Card({ data }) {
  return (
    <>
      <div>
        <div className="relative border border-gray-100" style={{ width: '300px' }}>
          <div className="relative object-cover w-full h-56 bg-slate-300">
            <Image
              src={data.image_url}
              alt={data.product_name}
              fill
              objectFit="cover"
              quality={80}
            />
          </div>
          <div className="p-6">
            <small>
              <span className="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-r-lg dark:bg-green-200 dark:text-green-900">
                {data.category.category_name}
              </span>
            </small>
            <h5 className="mt-4 truncate">{data.product_name}</h5>
            <ul className="mt-5 text-sm font-thin text-gray-500 ">
              <li>Stock : {data.stock}</li>
              <li className="text-lg font-bold">Harga : Rp {formatRp(data.price)}</li>
            </ul>

            <div className="flex items-center justify-between mt-4 border">
              <button className="h-full px-2 text-black bg-gray-200">-</button>
              <input
                className="inline-block w-full h-full text-center focus:outline-none"
                placeholder="1"
              />
              <button className="h-full px-2 text-black bg-gray-200">+</button>
            </div>
            <button
              className="block w-full p-4 mt-5 text-sm font-medium text-white bg-blue-500 border rounded-sm"
              type="button"
            >
              Add to Cart
            </button>

            <div
              className="block w-full p-4 mt-2 text-sm font-medium text-center text-blue-500 bg-white border border-blue-500 rounded-sm"
              type="button"
            >
              <Link href={`/detail-product/${data.id}`}>Detail Product</Link>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
