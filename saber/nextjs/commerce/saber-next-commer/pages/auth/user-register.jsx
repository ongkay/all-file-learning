import { Button, TextInput } from 'flowbite-react'
import Link from 'next/link'
import React, { useState } from 'react'
import { HiKey, HiMail, HiPhotograph, HiUser } from 'react-icons/hi'
import { useRouter } from 'next/router'
import fetchData from '../../services/api'

export default function Register() {
  const router = useRouter()
  const [input, setInput] = useState({
    name: '',
    email: '',
    image_url: '',
    password: '',
  })

  const handleChange = ({ target }) => {
    setInput({ ...input, [target.name]: target.value })
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    try {
      await fetchData.post('/register', input)
      router.push('/auth/user-login')
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <>
      <div className="flex items-center justify-center w-full h-screen">
        <div class="flex flex-col w-full max-w-md px-4 py-8 bg-slate-50 rounded-lg shadow-lg dark:bg-gray-800 sm:px-6 md:px-8 lg:px-10">
          <div class="self-center mb-6 text-xl font-light text-gray-600 sm:text-2xl dark:text-white">
            Register New Account
          </div>
          <form onSubmit={handleSubmit} className="flex flex-col gap-4">
            <TextInput
              id="name1"
              type="text"
              placeholder="Namamu"
              required={true}
              icon={HiUser}
              name="name"
              onChange={handleChange}
              value={input.name}
            />
            <TextInput
              id="email1"
              type="email"
              placeholder="Emailmu"
              required={true}
              icon={HiMail}
              name="email"
              onChange={handleChange}
              value={input.email}
            />
            <TextInput
              id="photomu"
              type="text"
              placeholder="Link Photomu"
              required={true}
              icon={HiPhotograph}
              name="image_url"
              onChange={handleChange}
              value={input.image_url}
            />
            <TextInput
              id="password1"
              type="password"
              placeholder="Paswordmu"
              required={true}
              icon={HiKey}
              name="password"
              onChange={handleChange}
              value={input.password}
            />

            <Button type="submit">Daftar Sekarang</Button>
          </form>

          <div class="flex items-center justify-center mt-6">
            <Link
              href="/auth/user-login"
              // target="_blank"
              class="inline-flex items-center text-base font-light text-center text-gray-500 hover:text-blue-800 dark:text-gray-100 dark:hover:text-white"
            >
              <span class="ml-2">Sudah Punya akun ? Login sekarang!</span>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
