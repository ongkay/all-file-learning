import Link from 'next/link'
import { useState } from 'react'
import Card from '../components/Card'
import Category from '../components/Category'
import fetchData from '../services/api'
import Layout from '../widget/Layout'
import { HiChevronDown } from 'react-icons/hi'
import { Button, Spinner } from 'flowbite-react'

export async function getServerSideProps({ req, res }) {
  // res.setHeader('Cache-Control', 'public, s-maxage=10, stale-while-revalidate=59')
  const { data: Product } = await fetchData('/product')

  return {
    props: {
      Product,
    },
  }
}

export default function Home({ Product }) {
  const [dataProduct, setDataProduct] = useState(Product)
  const [limit, setLimit] = useState(5)

  const [displaySpinner, setDisplaySpinner] = useState(false)

  const handleCounterFilter = () => {
    setDisplaySpinner(true)

    setTimeout(() => {
      setLimit(limit + 5)
      setDisplaySpinner(false)
    }, 500)
  }

  return (
    <>
      <Layout home>
        <div className="px-3 pt-20 my-10">
          <span className="font-bold text-blue-600">
            <Link href={`#`}>Lihat Semua Produk</Link>{' '}
          </span>
          <h1 className="mt-3 text-3xl font-bold">Produk Pilihan</h1>
        </div>
        <div className="px-3 mb-20">
          <Category />
        </div>
        <div className="flex flex-wrap items-center justify-center gap-10 lg:justify-start">
          {dataProduct.length !== 0 &&
            dataProduct
              .filter((res, index) => {
                return res.available === 1 && index < limit
              })
              .map((res) => {
                return <Card key={res.id} data={res} />
              })}
        </div>

        {!displaySpinner ? (
          <>
            <div className="container flex items-center justify-center mt-10">
              <Button outline={true} onClick={handleCounterFilter}>
                View More
                <HiChevronDown className="w-5 h-5 ml-2" />
              </Button>
            </div>
          </>
        ) : (
          <>
            <div className="container flex items-center justify-center mt-10">
              <Button>
                <div className="mr-3">
                  <Spinner size="sm" light={true} />
                </div>
                Loading ...
              </Button>
            </div>
          </>
        )}
      </Layout>
    </>
  )
}
