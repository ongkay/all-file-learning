import Image from 'next/image'
import { useRouter } from 'next/router'
import React from 'react'
import useFetch from '../../services/swr'
import formatRp from '../../utils/formatRp'
import Layout from '../../widget/Layout'

export default function DetailData() {
  const { id } = useRouter().query

  const { data: dataProduct, isLoading, isError } = useFetch(`/product/${id}`)

  if (isError) return <>SWR ERROR</>
  if (isLoading) return <>Loading...</>

  return (
    <Layout>
      <div className="container ">
        <h1 className="py-10 text-2xl font-bold">Detail Product</h1>
      </div>

      <div className="mt-10 border">
        <div className="flex-wrap w-full overflow-hidden bg-white rounded-lg shadow-lg sm:flex-none lg:flex">
          <div className="relative object-cover h-64 mb-5 w-96">
            <Image
              src={dataProduct.image_url}
              alt="produk"
              objectFit="cover"
              fill
              quality={80}
            />
          </div>
          <div className="w-full p-4 border lg:w-2/3 grow">
            <h1 className="text-2xl font-bold text-gray-900">
              {dataProduct.product_name}
            </h1>
            <p className="mt-2 text-sm text-gray-600">{dataProduct.description}</p>
            <div className="flex mt-2 item-center">
              <p className="text-sm">Stock : {dataProduct.stock}</p>
            </div>
            <div className="flex justify-between mt-3 item-center">
              <h1 className="text-xl font-bold text-gray-700">
                Rp {formatRp(dataProduct.price)}
              </h1>
              <button className="px-3 py-2 text-xs font-bold text-white uppercase bg-gray-800 rounded">
                Add to Card
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
