import useSWR from 'swr'

const fetcher = (url) => fetch(url).then((res) => res.json())
const urlAPI = 'https://service-example.sanbercloud.com/api'

export default function useFetch(endPoint) {
  const { data, error } = useSWR(`${urlAPI}${endPoint}`, fetcher)

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  }
}
