import axios from 'axios'

const urlAPI = 'https://service-example.sanbercloud.com/api'

const fetchData = axios.create({
  baseURL: urlAPI,
})

export default fetchData
