import React from 'react';
import Wallet from './pages/Wallet';
import ColorPage from './pages/ColorList';
import ColorForm from './pages/ColorForm';

function App() {
    return (
        <div className="App">
            <Wallet />
            <ColorPage />
            <ColorForm />
        </div>
    );
}

export default App;
