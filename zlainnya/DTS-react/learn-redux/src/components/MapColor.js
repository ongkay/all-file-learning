import React from 'react';

const MapColor = ({ item }) => {
    console.log(item.color);

    return (
        <div key={item.id} style={{ backgroundColor: item.color }}>
            {item.color}
        </div>
    );
};

export default MapColor;
