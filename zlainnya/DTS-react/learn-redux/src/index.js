import React, { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import store from './store/app/store';

import App from './App';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
    <StrictMode>
        {/* provide store yang kita punya dari redux */}
        <Provider store={store}>
            <App />
        </Provider>
    </StrictMode>,
);
