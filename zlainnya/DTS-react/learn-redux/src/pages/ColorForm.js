import React, { useState } from 'react';
import { useAddColorMutation } from '../store/colorFetch';
import MapColor from '../components/MapColor';

const ColorForm = () => {
    // Default value
    const defaultNewColor = {
        color: '',
    };

    const [newColor, setNewColor] = useState(defaultNewColor);

    // [1] useAddColorMutation
    const [addColor, { isLoading, data }] = useAddColorMutation();

    //handlechange dan click
    const handleChange = ({ target }) => {
        const { name, value } = target;

        setNewColor({
            ...newColor,
            [name]: value,
        });
    };

    const handleClick = async () => {
        addColor(newColor);
        setNewColor(defaultNewColor);
    };

    return (
        <>
            <MapColor item={data ? data : defaultNewColor} />

            <div style={{ margin: '20px' }}>
                <input
                    onChange={handleChange}
                    value={newColor.color}
                    name="color"
                    placeholder="hex color"
                />

                <button onClick={handleClick}>Create color</button>
                <p>Post result : </p>
                {/* hasil dari post bisa kita akses langsung di sini Kita gunakan conditional rendering */}

                {isLoading ? <>Sedang Loading...</> : <>{JSON.stringify(data)}</>}
            </div>
        </>
    );
};

export default ColorForm;

/**=====KETERANGAN=====
 * [1] hook dari mutation akan mengembalikan 2 hal :
 * - yang pertama adalah fungsi untuk mentrigger mutasi tersebut
 * - yg kedua respon yg berisi {data, error, isLoading, isError, isSuccess}
 */
