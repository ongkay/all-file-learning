/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    selectBalance,
    selectUser,
    setWithdraw,
    setDeposit,
    fetchUser,
} from '../store/wallet';

const Wallet = () => {
    /**AKSES STATE REDUX
     * untuk mengakses state yang kita buat di store, kita bisa pakai hook useSelector
     * useSelector akan menerima sebuah fungsi
     */
    const balance = useSelector(selectBalance);
    const user = useSelector(selectUser);

    const [userId, setUserId] = useState(1);
    const [inputJumlahnya, setInputJumlahnya] = useState(0);

    // kita pakai hooknya useDispatch agar bisa menggunakan fungsinya
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUser(userId));
    }, []);

    // kita memanggil dispatch dengan untuk set actionnya di state
    const onWithdraw = (jumlahnya) => {
        dispatch(setWithdraw({ jumlahnya }));
    };

    const onDeposit = (jumlahnya) => {
        dispatch(setDeposit({ jumlahnya }));
    };

    const onIdUserSubmit = () => {
        dispatch(fetchUser(userId));
    };

    return (
        <div>
            <h1>My Wallet</h1>
            <img src={user.avatar} alt="user avatar" />
            <h3>Name : {user.first_name}</h3>
            <input
                type="number"
                value={userId}
                onChange={({ target }) => setUserId(target.value)}
            />
            <button onClick={onIdUserSubmit}>submit user id</button>
            <br />
            <h3>Balance: Rp {balance.toLocaleString('ID')}</h3>
            <button onClick={() => onWithdraw(10000)}>Withdraw Rp 10.000</button>
            <button onClick={() => onDeposit(10000)}>Deposit Rp 10.000</button>
            <br />
            <br />
            <input
                type="number"
                value={inputJumlahnya}
                onChange={({ target }) => setInputJumlahnya(parseInt(target.value))}
            />
            <button onClick={() => onWithdraw(inputJumlahnya)}>withdraw</button>
            <button onClick={() => onDeposit(inputJumlahnya)}>deposit</button>
        </div>
    );
};

export default Wallet;
