import React from 'react';
import { useGetColorsQuery } from '../store/colorFetch'; // [1]
import MapColor from '../components/MapColor';

const ColorList = () => {
    const { data, error, isLoading } = useGetColorsQuery(); // [2]

    return (
        <div>
            <h1>ini data ColorList</h1>

            {/* Kita gunakan conditional rendering di sini */}

            {error ? (
                <>Ada error di sini nih ...</>
            ) : isLoading ? (
                <>Sedang Loading...</>
            ) : (
                <>
                    {data.data.map((item) => (
                        <MapColor key={item.id} item={item} />
                    ))}
                </>
            )}
        </div>
    );
};

export default ColorList;

/**=====KETERANGAN=====
 * [1] Import API yang di perlukan
 * [2] menggunakan hooks useColorsQuery
 *  - menerima Object seperti axios, jadi kita langsung deconstruct saja yah
 *  - Property yang akan kita ambil adalah:
 *  - - data: berisikan data yang di-query (response)
 *  - - error: berisikan informasi error yang didapatkan apabila terjadi error
 *  -  isLoading: berisikan kondisi apakah query masih dalam tahap "pending" atau tidak
 */
