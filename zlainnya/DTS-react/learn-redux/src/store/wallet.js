import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

// initialstate atau nilai defaultnya
const initialState = { user: {}, balance: 15000 };

/**====Buat createAsynThunk====*/
export const fetchUser = createAsyncThunk('wallet/fetch_user', async (userId) => {
    const { data: axiosData } = await axios.get(`https://reqres.in/api/users/${userId}`);

    return axiosData.data;
});

const walletSlice = createSlice({
    name: 'wallet',
    initialState,
    reducers: {
        /**===== REDUCER =====*/
        setWithdraw: (state, action) => {
            if (state.balance >= action.payload.jumlahnya) {
                state.balance -= action.payload.jumlahnya;
            } else {
                console.log('saldo gak cukup gileee');
            }
        },

        setDeposit: (state, action) => {
            state.balance += action.payload.jumlahnya;
        },
    },

    /**====Handle Promise=====*/
    extraReducers: (builder) => {
        builder
            .addCase(fetchUser.rejected, () => {
                console.log('Jika rejected atau gagal mau di apakan ?');
            })

            .addCase(fetchUser.pending, () => {
                console.log('ini posisi sedang loading, mau di apakan');
            })

            .addCase(fetchUser.fulfilled, (state, action) => {
                state.user = action.payload;
            });
    },
});

/**==== AUTO ACTIONS =====*/
export const { setDeposit, setWithdraw } = walletSlice.actions;

/**==== SELECTOR =====*/
export const selectBalance = (state) => state.wallet.balance;
export const selectUser = (state) => state.wallet.user;

export default walletSlice.reducer;
