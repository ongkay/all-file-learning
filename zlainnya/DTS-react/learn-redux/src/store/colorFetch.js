import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const colorApi = createApi({
    reducerPath: 'colorApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://reqres.in/api/',
    }),

    tagTypes: ['Color'],
    endpoints: (builder) => ({
        getColors: builder.query({
            query: () => 'colors',

            providesTags: ['Color'],
        }),

        addColor: builder.mutation({
            query: (color) => ({
                url: 'colors',
                method: 'POST',
                body: color,
            }),

            invalidatesTags: ['Color'],
        }),
    }),
});

export const { useGetColorsQuery, useAddColorMutation } = colorApi;
