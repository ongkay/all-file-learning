import { configureStore } from '@reduxjs/toolkit';
import walletReducer from '../wallet';
import { colorApi } from '../colorFetch';

const store = configureStore({
    reducer: {
        wallet: walletReducer,
        [colorApi.reducerPath]: colorApi.reducer, // ini RTK Query
    },

    // Menambahkan middleware untuk caching, invalidation, pooling
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(colorApi.middleware),
});

export default store;
