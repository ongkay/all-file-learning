imp→ import moduleName from 'module'
imn→ import 'module'
imd→ import { destructuredModule } from 'module'
exp→ export default moduleName
exd→ export { destructuredModule } from 'module'
