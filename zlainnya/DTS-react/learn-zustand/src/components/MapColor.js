import React from 'react';

const MapColor = ({ item }) => {
    return (
        <div key={item.id} style={{ backgroundColor: item.color }}>
            {item.color}
        </div>
    );
};

export default MapColor;
