import axios from 'axios';
import produce from 'immer';
import create from 'zustand';
import { createTrackedSelector } from 'react-tracked';
import { persist, devtools } from 'zustand/middleware';

const collorActions = (set) => {
    const setState = (fn) => set(produce(fn));
    const Loading = (status) => set({ isLoading: status });

    return {
        isLoading: false,
        colors: [],

        getColors: async () => {
            Loading(true);
            console.log('get Collor Rendered');

            const { data: axiosData } = await axios.get(`https://reqres.in/api/colors`);
            Loading(false);

            setState((state) => {
                state.colors = axiosData.data;
            });
        },

        addColor: async (newColor) => {
            console.log('addcollor render');
            Loading(true);

            const { data } = await axios.post(`https://reqres.in/api/colors`, newColor);
            Loading(false);

            setState((state) => {
                state.colors = [...state.colors, data];
            });
        },
    };
};

export const useColorStore = create(
    devtools(
        persist(collorActions, {
            name: 'color-storage',
            getStorage: () => sessionStorage,
        }),
    ),
);

// create selector
export const collorStoreSelector = createTrackedSelector(useColorStore);

// const useColorStore = create(
//     persist(collorActions, {
//         name: 'color-storage',
//         getStorage: () => sessionStorage,
//     }),
// );

// const useColorStore = create(collorActions);

// export default useColorStore;
