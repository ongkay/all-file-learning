import axios from 'axios';
import produce from 'immer';
import create from 'zustand';
import { createTrackedSelector } from 'react-tracked';
import { devtools, persist } from 'zustand/middleware';

const walletActions = (set) => {
    const setState = (fn) => set(produce(fn));

    return {
        balance: 15000,

        setDeposit: (jumlahnya) =>
            setState((state) => {
                state.balance = state.balance + jumlahnya;
            }),

        setWithdraw: (jumlahnya) =>
            setState((state) => {
                if (state.balance >= jumlahnya) {
                    state.balance = state.balance - jumlahnya;
                }
            }),
    };
};

const userActions = (set) => ({
    user: {},
    isLoading: false,

    fetchUser: async (userId) => {
        set({ isLoading: true });

        const { data: axiosData } = await axios.get(
            `https://reqres.in/api/users/${userId}`,
        );

        set({ user: axiosData.data, isLoading: false });
    },
});

// gabungkan semua action di sini
let combineStores = (set) => ({
    ...walletActions(set),
    ...userActions(set),
});

// persis untuk menyimpan data ke sessionstorage atau localstorage
combineStores = persist(combineStores, {
    name: 'walletUser',
    getStorage: () => sessionStorage,
});

// aktivekan devtools untuk debug
combineStores = devtools(combineStores);

// create zustand
export const useWalletStore = create(combineStores);

// create selector
export const walletStoreSelector = createTrackedSelector(useWalletStore);
