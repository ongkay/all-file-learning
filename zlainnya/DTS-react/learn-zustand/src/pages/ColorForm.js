import React, { useState } from 'react';
import { collorStoreSelector } from '../store/colorFetch';

const ColorForm = () => {
    // Default value
    const defaultNewColor = {
        color: '',
    };

    const [newColor, setNewColor] = useState(defaultNewColor);

    // selector
    const { addColor } = collorStoreSelector();

    //handlechange dan click
    const handleChange = ({ target }) => {
        const { name, value } = target;

        setNewColor({
            ...newColor,
            [name]: value,
        });
    };

    const handleClick = async () => {
        addColor(newColor);
        setNewColor(defaultNewColor);
    };

    return (
        <>
            <div style={{ margin: '20px' }}>
                <input
                    onChange={handleChange}
                    value={newColor.color}
                    name="color"
                    placeholder="hex color"
                />
                <button onClick={handleClick}>Create color</button>
            </div>
        </>
    );
};

export default ColorForm;
