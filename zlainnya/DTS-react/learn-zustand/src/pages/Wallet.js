/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { walletStoreSelector } from '../store/wallet';

// komponen khusus wallet
const ShowWallet = () => {
    const { balance, setDeposit, setWithdraw } = walletStoreSelector(); // auto selector

    const [inputJumlahnya, setInputJumlahnya] = useState(0);
    console.log('Wallet rendering');

    return (
        <div>
            <h3>Balance: Rp {balance.toLocaleString('ID')}</h3>
            <button onClick={() => setWithdraw(10000)}>Withdraw Rp 10.000</button>
            <button onClick={() => setDeposit(10000)}>Deposit Rp 10.000</button>
            <br />
            <br />
            <input
                type="number"
                value={inputJumlahnya}
                onChange={({ target }) => setInputJumlahnya(parseInt(target.value))}
            />
            <button onClick={() => setWithdraw(inputJumlahnya)}>withdraw</button>
            <button onClick={() => setDeposit(inputJumlahnya)}>deposit</button>
        </div>
    );
};

// komponen khusus user
const ShowUser = () => {
    const { user, fetchUser, isLoading } = walletStoreSelector(); // auto selector

    const [userId, setUserId] = useState(1);
    console.log('User rendering');

    useEffect(() => {
        fetchUser(userId);
    }, []);

    const onIdUserSubmit = () => {
        fetchUser(userId);
    };

    return (
        <div>
            <h1>My Wallet</h1>
            <img src={user.avatar} alt="user avatar" />
            <h3>Name : {user.first_name}</h3>

            <input
                type="number"
                value={userId}
                onChange={({ target }) => setUserId(target.value)}
            />
            {isLoading ? (
                <> Loading...</>
            ) : (
                <>
                    <button onClick={onIdUserSubmit}>submit user id</button>
                </>
            )}

            <br />
        </div>
    );
};

// render all komponen
const Wallet = () => {
    return (
        <>
            <ShowUser />
            <ShowWallet />
        </>
    );
};

export default Wallet;
