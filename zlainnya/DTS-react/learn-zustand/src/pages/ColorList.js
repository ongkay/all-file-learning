import React, { useEffect } from 'react';
import MapColor from '../components/MapColor';
import { collorStoreSelector } from '../store/colorFetch';

const ColorList = () => {
    // menggunakan selector shallow biar bisa banyak
    const { colors, isLoading, getColors } = collorStoreSelector();

    useEffect(() => {
        getColors();
    }, [getColors]);

    const handleClick = async () => {
        getColors();
    };

    return (
        <div>
            <h1>ini data ColorList</h1>
            {isLoading ? <>Loading broooo ...</> : <></>}

            {colors.map((item) => (
                <MapColor key={item.id} item={item} />
            ))}

            <button onClick={handleClick}>GET COLLOR</button>
        </div>
    );
};

export default ColorList;
