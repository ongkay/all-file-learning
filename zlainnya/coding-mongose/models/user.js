const mongoose = require ('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
  name:  String, // String is shorthand for {type: String}
  email: String,
  password:   String,

}, 
{ 
    timestamps: true //opsi untuk menjenerate 2 field 'createdAt' dan 'updatedAt'
});


const User = mongoose.model('User', userSchema);

module.exports = User
//impor ke user controllers




