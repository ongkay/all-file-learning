
// Buat index di field category di collection produk
db.products.createIndex({
    category: 1
});

// Cara melihat index. Get all indexes in products collection
db.products.getIndexes();

// Find products by category (will use index)
db.products.find({
    category: "food"
});


// Debugging query optimization
/**Untuk mengecek apakah benar menggunakan index
 * Jika sudah di buat index maka hasilnya akan terlihat "IXSCAN"
 * jika belum maka "COLLSCAN" artinya akan mencari data satu2 dan lama cuy
 */
db.products.find({
    category: "food"
}).explain();

db.products.find({}).sort({
    category:1
}).explain();



 
/**Jika membuat index lebih dari 1 maka
 * field 1 --> kena
 * field 1, 2 --> kena
 * field 1, 2, 3 --> kena
 * field 2 --> tidak
 * field 2, 3 --> tidak
 */

// Create index at price and tags in products collection
db.products.createIndex({
    stock: 1, // field 1
    tags: 1 // field 2
});

// Find products by stock and tags (will use index)
db.products.find({
    stock: 10,
    tags: "popular"
});

// Debugging query optimization
db.products.find({
    stock: 10
}).explain();
db.products.find({
    stock: 10,
    tags: "popular"
}).explain();
db.products.find({
    tags: "popular"
}).explain();

