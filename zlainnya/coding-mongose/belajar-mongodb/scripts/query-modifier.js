
/**menampilkan berapa jumlah data yang data dicollection produk
 * akan di tampilkan angka, misalnya 3, 8
 * SQL :
 */
// select count(*) from products
db.products.find({}).count()


/**menampilkan semua data produk sebanyak 4 
 * SQL :
 */
// select * from products limit 4
db.products.find({}).limit(4)


/** tampilkan semua data produk urutan 2 pertama tidak di tampilkan
 * SQL :
 */
// select * from products offset 2
db.products.find({}).skip(2)


/**tampilkan semua data produk sebanyak 4 produk dan lewati 2 urutan pertama
 * SQL :
 */
// select * from products limit 4 offset 2
db.products.find({}).limit(4).skip(2)


/** tampilkan semua data produk 
 * field 'name' urutkan asc (a-z) dan
 * field 'category' urukan desc (z-a)
 * SQL :
 */
// select * from products order by name asc, category desc
db.products.find({}).sort({
    name: 1, //asc
    category: -1 //desc
})