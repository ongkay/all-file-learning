/**contoh menampilkan semua produk yang ada di katagori "Laptop dan Hanphone dengan harga diatas 20000000" 
 * SQL : select * from products where category in ('laptop', 'handphone') and price > 20000000
*/
db.products.find({
    $and: [
        {
            category: {
                $in: ["laptop", "handphone"]
            }
        },
        {
            price: {
                $gt: 20000000
            }
        }
    ]
});

/**ingin mencari/menampilkan produk yang katagorinya tidak didalam "laptop dan hanphone" 
 * SQL : select * from products where category not in ('laptop', 'handphone')
*/
db.products.find({
    category: {
        $not: {
            $in: ["laptop", "handphone"]
        }
    }
});

/**ingin mencari produk  yang harganya 1000 hingga 20000000 
 * dan katagorinya tidak sama dengan 'food'
 * SQL : select * from products where price between 10000000 and 20000000 and category != 'food'
*/
db.products.find({
    $and: [
        {
            price: {
                $gte: 1000,
                $lte: 20000000
            }
        },
        {
            category: {
                $ne: 'food'
            }
        }
    ]
});