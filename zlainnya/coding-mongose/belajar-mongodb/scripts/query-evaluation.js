//Mencari produk yang field price diatas 1000000
// select * from products where price > 1000000
db.products.find({
    $expr: {
        $gt: ["$price", 1000000]
    }
});

// mencari produk dengan uppercasin dulu field _id
// select * from customers where toUpper(_id) = 'KHANNEDY'
db.customers.find({
    $expr: {
        $eq: [
            { $toUpper: "$_id" }, 
            "KHANNEDY"
        ]
    }
});

//mengambil produk yang wajib ada field 'name' dan 'catagory'
// select * from products where name is not null and category is not null
db.products.find({
    $jsonSchema: {
        required: [ "name", "category"]
    }
});

//contoh lebih detail lagi
// select * from products where name is not null and type(name) = 'string' and type(price) = 'long'
db.products.find({
    $jsonSchema: {
        required: [ "name"],
        properties: {
            name: {
                bsonType: "string"
            },
            price: {
                bsonType: "long"
            }
        }
    }
});

/**mencari produk dengan harga yg habis di bagi 5
 * select * from products where price % 5 = 0
**/
db.products.find({ 
    price: { 
        $mod: [5, 0]
    } 
});

/**Mencari produk yang mengandung kata 'mie
 * SQL : select * from products where name like "%mie%"
 */
db.products.find({
    name: {
        $regex: /mie/,
        $options: "i"
    }
});

/**Mencari produk dengan katakunci yang diawali dengan 'Mie'
 * SQL : select * from products where name like "Mie%"
 */
db.products.find({
    name: {
        $regex: /^Mie/
    }
});


/**Mencari Produk dengan Index
 * SQL : create text index on products
 */
db.products.createIndex({
    name: "text",
    catagory: "text"
});

//dengan membuat index di atas, maka pencarian hanya didalam field 'name' dan 'catagory'

/**Mencari produk dengan katakunci yang mengandung kata 'mie' ATAU 'sedap' 
 * di field index diatas
 * SQL : select * from products where (name like "%mie%" or name like "%sedap%")
 */
db.products.find({
    $text: {
        $search: "mie sedap" //==> mie atau sedap
    }
});

/**Mencari produk dengan katakunci yang mengandung kata 'mie' DAN 'sedap' 
 * di field index diatas
 * SQL : select * from products where name like "%mie sedap%"
 */
db.products.find({
    $text: {
        $search: '"mie sedap"' //==> mie dan sedap
    }
});

/**Mencari ID menggunakan functions
 * SQL : select * fro customers where _id = "khannedy"
 */
db.customers.find({
    $where: function(){
        return this._id == "khannedy";
    }
});