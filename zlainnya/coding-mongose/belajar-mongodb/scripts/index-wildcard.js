// membuat wildcard index
/**Semua data yg ada di field "customFields" akan ter index
 * jadi gak perlu buat idex satu persatu
 * cuku buat gini aja
 */
db.customers.createIndex({
    "customFields.$**" : 1
});



// Insert many customers
db.customers.insertMany([
    {
        _id: "budi",
        full_name: "Budi",
        customFields: {     // semua data di field ini otomatis ter index
            hobby: "Gaming",
            university: "Universitas Belum Ada"
        }
    },
    {
        _id: "joko",
        full_name: "Joko",
        customFields: {
            ipk: 3.2,
            university: "Universitas Belum Ada"
        }
    },
    {
        _id: "rudi",
        full_name: "Rudi",
        customFields: {
            motherName: "Tini",
            passion: "Entepreneur"
        }
    }
])

// Debug wildcard index
db.customers.find({
    "customFields.passion": "Enterpreneur"
}).explain();

db.customers.find({
    "customFields.ipk": 3.2
}).explain();

db.customers.find({
    "customFields.hobby": "Gaming"
}).explain();