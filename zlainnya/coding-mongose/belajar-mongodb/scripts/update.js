
/**Misalnya ingin mengupdate atau menambahkan field catagory di colection products _id1
 * SQL : update products set category = "food" where _id = 1
 */
db.products.updateOne({
    _id: 1
},{
    $set: {
        category: "food"
    }
});


/**update atau menambahkan field catagory di colection products _id2
 * SQL : update products set category = "food" where _id = 2
 */
db.products.updateOne({
    _id: 2
},{
    $set: {
        category: "food"
    }
});


/**menambahkan field 'tags' dengan value food di produk yang
 * ada di field 'category food' DAN yg belum memiliki field 'tags'
 * SQL : update products set tags = ["food"] where category = "food" and tags is null
 */
db.products.updateMany({
    $and: [
        {
            category :{
                $eq: "food"
            }
        },
        {
            tags: {
                $exists: false
            }
        }
    ]
}, {
    $set: {
        tags: ["food"]
    }
})


/**Menambahkan field 'wrong' ke semua produk
 * SQL : update products set wrong = "wrong"
 */
db.products.updateMany({}, [
    {
        $set: {
            wrong: "wrong"
        }
    }
]); 


/**Menghapus field 'wrong' di semua produk
 * SQL : update products set wrong = null
 */
db.products.updateMany({}, [
    {
        $set: {
            wrong: null
        }
    }
]);
db.products.updateMany({}, [
    {
        $unset: [ "wrong" ] 
    }
]);


/**Menimpa semua isi data produk di _id: 9
 * semua data akan berubah baru
 * benar-benar menimpah bulat-bulat tanpa ampun cuy...
 * SQL :  replace document with id 9
 */
db.products.replaceOne({
    _id: 9
}, {
    name: "Adidas Pulseboost HD Running Shoes Sepatu lari Pria",
    price: new NumberLong(1100000),
    category: "shoes",
    tags: [
        "adidas", "shoes", "running"
    ]
});