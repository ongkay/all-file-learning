// contoh cari _id :
db.customers.find({
    _id: "khannedy"
});

// contoh cari name :
db.customers.find({
    name: "Eko Kurniawan Khannedy"
});

// contoh cari harga :
db.products.find({
    price: 2000
});

// akses embeded objek
db.orders.find({
    "items.product_id": 1
});

// contoh cari name lebih dari satu :
db.customers.find({
    _id: "khannedy",
    name: "Eko Kurniawan Khannedy"
});