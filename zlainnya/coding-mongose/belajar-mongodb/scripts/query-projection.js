
/**Menampilkan semua produk yang ada field 'name' dan 'category'
 * pasti yg di tampilkan hanya field 'name' dan 'category' saja
 * field lainnya seperti 'price', 'tags' dll tidak akan di tampilkan cuy.
 * SQL : select _id, name, category from products
 */
// 
db.products.find({}, {
    name: 1,
    category: 1
});


/**Menampilkan semua produk kecuali yang tetapi field 'tags' tidak akan di tampilkan
 * semua field akan ditampilkan kecuali 'tags'
 * SQL : select _id, name, category, price from products
 */
// 
db.products.find({}, {
    tags: 0
});


/**menampilkan semua produk yang memiliki field tags aja
 * yang mengandung element 'samsung' ATAU 'logitech'
 * dan yang di tampikan hanya field : name, category, price. dan tags
 * SQL : select _id, name, category, price, tags[first] from products where tags in ("samsung", "logitech")
 */
// 
db.products.find({
    tags: {
        $elemMatch: {
            $in: ["samsung", "logitech"]
        }
    }
}, {
    name: 1,
    category: 1,
    price: 1,
    "tags.$": 1 //Akan di ambil tags yang pertama aja, sisanya gak akan di ambil
});


/**menampilkan semua produk
 * tetapi yang ditampilkan/include hanya field : name, category, price dan
 * field tags hanya di tampilkan yang mengandung element 'samsung' ATAU 'logitech'
 * SQL : select _id, name, category, price, tags(in ("samsung", "logitech")) from products
 */
// 
db.products.find({}, {
    name: 1,
    category: 1,
    price: 1,
    tags: {
        $elemMatch: {
            $in: ["samsung", "logitech"]
        }
    }
})


/**Menampilkan produk yang mengandung kata 'monitor'
 * dan cek scorenya.
 * Score paling tinggi adalah 1
 * SQL : select *, score from products where $search like "monitor"
 */
// 
db.products.find({
    $text: {
        $search: "monitor"
    }
},{
    score: {
        $meta: "textScore"
    }
})


/**Menampilkan semua produk 
 * tetapi field 'tags' hanya di ambil 2 element urutan dari depan
 * SQL : select _id, name, price, category, tags[0,2] from products
 */
// 
db.products.find({}, {
    tags: {
        $slice: 2
    }
});


/**Menampilkan semua produk 
 * tetapi field 'tags' hanya di ambil 2 element urutan dari belakang
 * SQL : select _id, name, price, category, tags[last 2] from products
 */
// 
db.products.find({}, {
    tags: {
        $slice: -2
    }
});


/**Menampilkan semua produk 
 * tetapi field 'tags' hanya di ambil di index 1 dan di tampikan 1 element
 * SQL : select _id, name, price, category, tags[from, limit] from products
 */
// 
db.products.find({}, {
    tags: {
        $slice: [1, 1] // [index, brapa banyak]
    }
});