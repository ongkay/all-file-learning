
/**untuk praktekan, kita Update/tambahkan dahulu field ratings di semua produk
 * SQL : update products set ratings = [90, 80, 70]
 */
db.products.updateMany({}, {
    $set: {
        ratings: [90, 80, 70]
    }
});


/**Merubah rating pertama 90 menjadi 100
 * SQL : update first element of array, query must include array fields
 */
db.products.updateMany({ 
    ratings: 90
}, {
    $set: {
        "ratings.$": 100
    }
});


/**Merubah semua isi field 'rating' menjadi 100
 * contohnya isinya : 90, 80, 70
 * maka setelah di set akan berubah : 100, 100, 100
 * SQL : update all element of array
 */
db.products.updateMany({}, {
    $set: {
        "ratings.$[]": 100
    }
})


/**
 * SQL : update products set ratings = [90, 80, 70]
 */
db.products.updateMany({}, {
    $set: {
        ratings: [90, 80, 70]
    }
});


/**contohnya ingin update nilai yg ada di field 'rating' menjadi 100 khusus
 * untuk rating yg di atas 80 saja, kalo di bawah 80 maka tidak di update.
 * SQL : update element of array based on arrayFilters
 */
db.products.updateMany({}, {
    $set: {
        "ratings.$[element]" : 100 //nilai updatenya
    }
},{
    arrayFilters: [ 
        { 
            element: {  //opsinya disini
                $gte: 80
            } 
        } 
    ]
})


/**update index ke-1 dan kedua di semua field 'rating'
 * SQL : update element of array with given index
 */
db.products.updateMany({}, {
    $set: {
        "ratings.0": 50, //index ke-1
        "ratings.1": 60 // index ke-2
    }
});











/**menambahkan field tags ke id1 dengan value tags 'populer'
 * otomatis akan update jika datanya belum ada, 
 * jika sudah ada maka tidak akan bertambah cuy...
 * SQL : add "popular" to array if not exists
 */
db.products.updateOne({
    _id: 1
}, {
    $addToSet: {
        tags: "popular"

    }
});


/**Menghapus data ratings element yg pertama
 * SQL : remove first element of array
 */
db.products.updateMany({}, {
    $pop: {
        ratings: -1 // element pertama : -1, element terakhir : 1
    }
});


/**Ngeset lagi
 * SQL : update products set rating = [90, 80, 70]
 */
db.products.updateMany({}, {
    $set: {
        ratings: [90, 80, 70]
    }
});


/**Menghapus semua rating yang diatas 80
 * SQL : remove all element where ratings >= 80
 */
db.products.updateMany({}, {
    $pull: {
        ratings: {
            $gte: 80
        }
    }
})


/**menambahkan rating 100 di semua produk 
 * akan bertambah paling akhir atau idex terakhir
 * SQL : add 100 to ratings
 */
db.products.updateMany({}, {
    $push: {
        ratings: 100
    }
})


/**menhapus rating 100 dan juga 40
 * SQL : remove element 100 
 */
db.products.updateMany({}, {
    $pullAll: {
        ratings: [100, 40]
    }
})







/**menambahkan rating di semua produk sebanyak 3 element
 * nilai rating 100, 200, 300
 * SQL : add 100, 200, 300 to ratings
 */
db.products.updateMany({},{
    $push: {
        ratings: {
            $each: [100, 200, 300]
        }
    }
})


/**menambahkan tags baru sebanyak 2 di semua produk
 * tetapi tags nya harus unix 
 * jika duplicat maka akan di skip
 * SQL : add trending, popular to tags
 */
db.products.updateMany({},{
    $addToSet: {
        tags: {
            $each: ["trending", "popular"]
        }
    }
})


/**menambahkan tags 'hot' di semua produk di posisi index ke-1
 * akan masuk ke element kedua index ke 1
 * SQL : add hot in posititon 1
 */
db.products.updateMany({},{
    $push: {
        tags: {
            $each: ["hot"],
            $position: 1
        }
    }
})


/**menambahkan element rating maksimal 2
 * dan ambil 2 dari urutan paling belakang
 * data akan tertimpa jadi baru
 * SQL : add all element, but limit with slice
 */
db.products.updateMany({},{
    $push: {
        ratings: {
            $each: [150, 200, 300, 400, 500],
            $slice: -2 // max 2, mengambil urutan paling belakang : 400, 500
        }
    }
})
db.products.find()

/**Menambahkan element rantings baru secara desending
 * SQL : add all element, and sort desc
 */
db.products.updateMany({},{
    $push: {
        ratings: {
            $each: [300, 400, 500],
            $sort: -1 // menambahkan data baru desending : 500, 400, 300
        }
    }
})
