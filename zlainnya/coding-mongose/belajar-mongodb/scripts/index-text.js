

// drop current index text
db.products.dropIndex("name_text");

// create index text
db.products.createIndex({
    name: "text",
    category: "text",
    tags: "text"
}, {
    weights: {
        name: 10, // nilai semakin besar akan semakin di prioritaskan
        category: 5,
        tags: 1
    }
});


// search products with text "mie"
/**ketika sudah di buat index text, maka kita quary search 
 * akan mencari data yg sdh di index (name, category, tags)
 */
db.products.find({
    $text: {
        $search: "mie"
    }
});

// search products with text "mie" OR "laptop"
/**jka ada 2 kata di search kayak gini maka di bacanya OR */
db.products.find({
    $text: {
        $search: "mie laptop" // dibaca : mie OR Laptop
    }
});

// search products with text "mie sedap"
/**nah kalo mau 2 kata di baca 1 maka tambahkan `""` */
db.products.find({
    $text: {
        $search: '"mie sedap"' // dibaca : mie sedap
    }
});

// search products with text "mie" and NOT "sedap"
/**mencari kata "mie" tetapi tidak boleh ada kata "sedap" */
db.products.find({
    $text: {
        $search: "mie -sedap"
    }
});


// Debugging query optimization
db.products.find({
    $text: {
        $search: "mie -sedap"
    }
}).explain();