/* eslint-disable */

export { }; // quick fix for global variable

/**
 * Partial => Partial<T> --> buat jadi opsional
 * Required => Required<T> --> buat jadi wajib disini semua
 * Readonly => Readonly<T> --> buat jadi opsiona
 * Pick => Pick<T, K> --> memilih prop yg kita inginkan
 * Omit => Omit<T, K> --> kebalikan dri pict, untuk kecualikan prop
 */

// use case : transform existing interface to new interface without modify existing interface

interface User {
  id: number;
  name: string;
  age?: number;
  createdAt: Date;
}

// function CreateUser(data: User) {}
// function CreateUser(data: Readonly<User>) { }
// function CreateUser(data: Partial<User>) { }
// function CreateUser(data: Required<User>) { }
// function CreateUser(data: Pick<User, 'id' | 'name'>) { }
function CreateUser(data: Omit<User, 'age' | 'createdAt'>) { }

CreateUser({
  id: 1,
  name: 'doe',
  // age: 20,
  // createdAt: new Date(),
});

/**
 * Record  => Record<K,T>
 */
/*

/*
usecase : object of object
  {
    propA: {id,name,age},
    propB: {id,name,age}
  }
*/



/**
 * Extract => Extract<T, U> --> mengambil/mencari property yang sama
 * Exclude => Exclude<T, U> ---> mengambil property yang tidak sama
 */

interface BioUser {
  id: number;
  name: string;
  age?: number;
  createdAt: Date;
}

interface Post {
  id: number;
  title: string;
  createdAt: Date;
}

// Extract => Extract<T, U> --> mengambil/mencari property yang sama
type ExtractType = Extract<keyof BioUser, keyof Post>

let iniku: ExtractType;
iniku = "id"













