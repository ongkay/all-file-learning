{//basic type data 
    // string
    let nama: string;
    nama = 'saya';

    // number
    let umur: number
    umur = 19;

    // boolean
    let isMarried: boolean
    isMarried = true
    isMarried = false

    // any
    let heroes: any;
    heroes = "mantap"
    heroes = 123
    heroes = ["mantap"]

    // type aliases
    type CustomeTipe = string;
    let mytipe: CustomeTipe;
    mytipe = 'hello'

    // union --> menetapkan banyak typedata
    let multi: string | number;
    multi = 'hello'
    multi = 123


    // ==================================

    {    // Enum =============================
        // jika enum string maka isinya harus string semua, begitu juga sebaliknya
        enum Month {
            JAN = 'Januari',
            FEB = 'Februari',
            MAR = 'Maret',
            APR = 'April',
        }
        console.log(Month.APR) //April
    }

    {// extends interface
        interface BasicAddress {
            name: string;
            street: string;
            city: string;

        }

        interface AddressWithUnit extends BasicAddress {
            country: string;
            postalCode?: string;
            unit?: string;
        }

        let resul: AddressWithUnit;
        resul = {
            name: "siapa",
            street: "jalan jalan",
            city: "bandung",
            country: "amerik"
        }
    }
}

{// Array 
    let isArray: []
    isArray = [];

    // Array of number square brackets
    let arrayOfNumber: number[]
    arrayOfNumber = [1, 2, 3]

    // Array of string square brackets
    let arrayOfString: string[]
    arrayOfString = ['mantap', "super", "saya"]

    // Generic Array
    let fruitArray: Array<string> = ['apple', 'orange', 'banana']

    // Array of bebas square brackets
    let arrayOfAny: any[]
    arrayOfAny = ['mantap', 123, true]

    // Array Tuples ==> isinya berbagaimacam type data tapi isinya terbatas
    let biodata: [string, number, number];
    biodata = ['surabaya', 123, 145]


    {// array union string or number withcsquare brackets
        let list01: (string | number)[]
        list01 = ['hello', 123]

        // array union with generic array
        let list02: Array<string | number>
        list02 = ['hello', 123]
    }

    {// Array of object
        let arjec: {}[]

        // -  inline interface
        let warnaku: { color: string, index: number, hex: number }[]
        warnaku = [{ color: 'blue', index: 1, hex: 123 }]

        //- with generic Array
        let warnaku02: Array<{ color: string, index: number, hex: number }>
        warnaku02 = [{ color: 'blue', index: 1, hex: 123 }]
    }

    {//  Array multi dimension [[]]
        let matrix: number[][];
        matrix = [
            [1, 2, 3],
            [4, 5]
        ];

        // with generic
        let matrix2: Array<Array<number>>
        matrix2 = [
            [1, 2, 3],
            [4, 5]
        ];
    }

    // Destructuring
    let [a, b, c, d]: [number, number, number, string] = [1, 2, 3, 'empat']
}

{// OBJECT ================

    {//Object Literal
        //[1] menggunakan type aliases
        type DataUser = {
            name: string;
            age: number;
            adress?: string;
        }

        let dataUser: DataUser;
        dataUser = {
            name: "Saya",
            age: 20
        }

        //[2] menggunakan inline interface
        interface DataUser2 {
            name: string;
            age: number;
            adress?: string;
        }
        let dataUser2: DataUser2;
        dataUser2 = {
            name: "Saya",
            age: 20
        }
    }

    {// Nested Object
        // [1] interface Adress
        interface Address {
            street: string;
            city: string;
        }

        // [2] interface User
        interface User {
            id: string;
            name: string;
            address: Address;
        }

        // [3] Implementasi :
        let user: User;
        user = {
            id: "U-1",
            name: "Adi dodi",
            address: {
                street: "Jln. Setapak No.2",
                city: "Jakarta"
            },

        }
    }

    {// Nested Object of Array
        // [1] interface Adress
        interface Adress {
            street: string;
            city: string;
        }

        // [2] interface User
        interface User {
            id: string;
            name: string;
            address: Array<Adress>;
        }

        // [3] Implementasi :
        let user: User;
        user = {
            id: "U-1",
            name: "Adi dodi",
            address: [
                {
                    street: "Jln. Setapak No.2",
                    city: "Jakarta"
                },
                {
                    street: "Jln. Lebar sekali no 10",
                    city: "Medan"
                }
            ]
        }
    }

    {// Nested Object of Object --> Gabungan multi object
        //[1] interface cart item
        interface CartItem {
            id: string;
            name: string;
            qty: number;
        }

        //[2] interface Cart detail (mengabungkan interface cartitem)
        interface Cart {
            idCart: string;
            DateOrdered: Date;
            items: {
                [key: string]: CartItem // agar propertynya dinamis maka kita gunakan [key: string]
            };
        }

        //[3] implementasi:
        let keyboard: Cart;
        keyboard = {
            idCart: 'ce323',
            DateOrdered: new Date('2023-05-20'),
            items: {
                p1: {
                    id: 'p-1',
                    name: "Miww keyboard",
                    qty: 2
                },
                p2: {
                    id: 'p-2',
                    name: "asus keyboard",
                    qty: 1
                }
            }
        }
    }

    {// Object Destructuring
        let fullName = {
            firstName: "Sastra",
            lastName: "Nababan"
        }

        // menggunakan type interface
        let { firstName, lastName } = fullName

        {// menggunakan Inline interface
            let { firstName, lastName }: { firstName: string; lastName: string } = fullName
        }
    }
}

{ // function ================================
    // mengembalikan return string, jika tidk maka akan error :
    function getName(): string {
        return "fungtion ini kembalian string"
    }
    console.log(getName())

    // jika tidak ada return maka Void, jika ada return maka akan error
    function printName(): void {
        console.log(getName())

    }
    printName()

    // jika ada arguments maka langsung di set type datanya di argument tersebut
    function sum(a: number, b: number): number {
        return a + b
    }
    console.log(sum(2, 4)) //  6

    // function as type
    let g: () => void;
    g = function () {
        return null;
    };

    //atau
    type Tambah = (val1: number, val2: number) => number;
    const add: Tambah = (val1: number, val2: number): number => {
        return val1 + val2
    }
    console.log(add(100, 500)) //600


    // default Parameter
    const fullname = (first: string, last: string = "Sutarno", adress?: string): string => {
        return first + " " + last
    }
    console.log(fullname("Sijar")) // Sijar Sutarno
    console.log(fullname('Sijar', 'moko')) // Sijar Sutarno

}

{/** class =====================
 * public = bisa di akses di semua class / dari luar class (default)
 * protected = hanya bisa di akses dari class tersebut, dan kelas turunannya
 * private = hanya bisa di akses dari class itu sendiri
 * readonly = hnya bisa membaca tetapi tidak akan bisa di edit
 */

    //class dasar
    {
        class User1 {
            name: string;

            constructor(name: string) {
                this.name = name
            }
        }

        const result1 = new User1('saya')
        console.log(result1.name) // saya

        // atau bisa lebih sempel seperti ini
        class User2 {
            constructor(public name: string) {
            }
        }

        const result2 = new User2('saya')
        console.log(result2.name) // saya

        {// contoh class dengan readonly
            class Namaku {
                readonly nama: string;
                pekerjaan: string = 'programmer';

                constructor(theName: string) {
                    this.nama = theName;
                }
            }

            let namaku = new Namaku("Andri");
            namaku.pekerjaan = "bisnismen";
            // namaku.nama = "Joni"; // tidak bisa di rubah
        }
    }

    // Inheritance
    // [1] ini adalah class utama kita
    class Users {
        name: string;
        age: number;

        constructor(name: string, age: number) {
            this.name = name
            this.age = age
        }

        //method
        setName(value: string): void {
            this.name = value
        }
        //method
        getName = (): string => { return this.name }

    }

    {// [2] class turunan tanpa menambahkan parameter baru
        class Admin extends Users {
            read: boolean = true;
            write: boolean = true;

            getRole(): { read: boolean, write: boolean } {
                return {
                    read: this.read,
                    write: this.write
                }
            }
        }
        // result 
        let admin = new Admin('toni', 25)
        admin.getName() // tonui
        admin.getRole() // { read: true, write: true }
        admin.setName('siapa') // siapa
    }

    {// [3] Super contructor -->  class turunan dengan menambahkan parameter baru
        class Admin2 extends Users {
            read: boolean = true;
            write: boolean = true;
            phone: string; // ini adalah variabel baru 

            constructor(name: string, age: number, phone: string) {
                super(name, age) // gunakan super agar bisa di gabungkan dengan variabel baru
                this.phone = phone
            }

            getRole(): { read: boolean, write: boolean } {
                return {
                    read: this.read,
                    write: this.write
                }
            }
        }
        // result 
        let admin2 = new Admin2('toni', 25, "081995970069")
        admin2.getName() // tonui
        admin2.getRole() // { read: true, write: true }
        admin2.setName('siapa') // siapa
    }

    {// [3] Setter and Getter 
        class Admin3 extends Users {
            read: boolean = true;
            write: boolean = true;
            phone: string;
            private _email: string = ""; // tidak akan bisa di akses kecuali di class ini aja

            constructor(name: string, age: number, phone: string) {
                super(name, age)
                this.phone = phone
            }

            // Set --> untuk mengubah nilai _email yg kita private. disini kita buat validasi saja
            set email(value: string) {
                if (value.length < 5) {
                    this._email = "email yang anda masukkan salah"
                } else {
                    this._email = value
                }
            }

            // Get --> untuk mendapatkan nilai email tersebut, sehingga akan tampil property Email
            get email(): string {
                return this._email
            }

            getRole(): { read: boolean, write: boolean } {
                return {
                    read: this.read,
                    write: this.write
                }
            }
        }
        // result
        let admin3 = new Admin3('toni', 25, '081995970069')
        admin3.getName() // toni
        admin3.getRole() // { read: true, write: true }
        admin3.setName('siapa') // siapa
        admin3.email = "iniemailnya@gmail.com" // set
        console.log(admin3.email) // get
    }

    {// [4] Static Property
        class Admin4 extends Users {
            read: boolean = true;
            write: boolean = true;
            phone: string;
            private _email: string = "";
            static getRoleName: string = 'Admin'

            constructor(name: string, age: number, phone: string) {
                super(name, age)
                this.phone = phone
            }

            static getNameRole() {
                return "hai"
            }

            // Set --> untuk mengubah nilai _email yg kita private. disini kita buat validasi saja
            set email(value: string) {
                if (value.length < 5) {
                    this._email = "email yang anda masukkan salah"
                } else {
                    this._email = value
                }
            }

            // Get --> untuk mendapatkan nilai email tersebut, sehingga akan tampil property Email
            get email(): string {
                return this._email
            }

            getRole(): { read: boolean, write: boolean } {
                return {
                    read: this.read,
                    write: this.write
                }
            }
        }
        // result --> bisa langsung akses tanpa buat instence new admin4
        let admin4 = Admin4.getNameRole()
        console.log(admin4) // hai
    }

    {// Abstract Class
        // [1] kita akan buat class bernama kendaraan yang wajib memiliki roda dan tombol start
        abstract class Kendaraan {
            abstract roda: number;

            start(): void {
                console.log("bruummmm")
            }
        }

        // [2] buat class mobil yang instence dari kendaraan
        class Mobil extends Kendaraan {
            roda: number = 4; // wajib ada jika sesuai di abstract class, tidak ada maka akan error
            spion: number = 2; // nah selanjutnya menambahkan yg optional
        }

        // [2] buat juga class motor yang instence dari kendaraan dengn jumlah roda yg berbeda
        class Motor extends Kendaraan {
            roda: number = 2
        }

        // result
        let mobil = new Mobil();
        console.log(mobil.roda); // 4
        console.log(mobil.spion); // 4
        mobil.start(); // bruummmm

        let motor = new Motor();
        console.log(motor.roda); // 2
        motor.start(); // bruummmm
    }

    {// Class with Interface --> membuat blueprint terlebih dahulu
        // [1] buat interfece telebih dahulu
        interface Laptop {
            name: string;
            on(): void;
            off(): void;
        }

        // cara menggunakan setiap ada yang ada di interface maka wajib ada di dlm class tersebut
        class Asus implements Laptop {
            name: string;
            isGaming: boolean;
            constructor(name: string, isGaming: boolean) {
                this.name = name;
                this.isGaming = isGaming;
            }

            on(): void {
                console.log("nyala")
            }
            off(): void {
                console.log("mati")
            }

        }
        let asus = new Asus('Asus', true)
        console.log(asus.on()) // nyala
        console.log(asus.off()) // mati

        // macbook adalah bagian dari laptop
        class MacBook implements Laptop {
            name: string;
            keyboardLight: boolean;
            constructor(name: string, keyboardLight: boolean) {
                this.name = name;
                this.keyboardLight = keyboardLight;
            }

            on(): void {
                console.log("nyala")
            }
            off(): void {
                console.log("mati")
            }

        }
        let macBook = new MacBook('MBP', true)
        console.log(macBook.on()) // nyala
        console.log(macBook.off()) // mati
    }


    {// GENERIC
        //[1] jika tanpa menggunakan generic maka :
        function getDataAny(value: any) {
            return value
        }

        //[2] setelah menggunakan generic
        function getData<T>(value: T) {
            return value
        }

        //[3] jika arrowFunc :
        const getDataArrow = <T,>(value: T) => {
            return value
        }

        // result sama semua
        console.log(getDataAny("dataku")) // dataku
        console.log(getData("dataku")) // dataku
        console.log(getDataArrow("dataku")) // dataku
    }

    {// class Generic
        class List<T> {
            private data: T[];
            constructor(...elements: T[]) {
                this.data = elements;
            }

            add(elements: T): void {
                this.data.push(elements)
            }

            addMultiple(...elements: T[]): void {
                this.data.push(...elements)
            }

            getAll(): T[] {
                return this.data
            }
        }
        // cara menggunakan : jika menggunakan generic, wajib menentukan type manual :
        let random = new List<number | string>(1, "b", 'c', 5)
        random.add('sdkjfks')
        random.add('kedua')
        random.addMultiple(123, 124)
        console.log(random.getAll()) // [ 1, 'b', 'c', 5, 'sdkjfks', 'kedua', 123, 124 ]
    }

    {// Indexed access type
        type Binatang = {
            nama: string
            kaki: number
            tanduk: Boolean
            skills: {
                lari: boolean
                melompat: boolean
                berenang: boolean
            }
        }

        // Jika ingin akses type skills dari binatang di atas maka hanya buat :
        type SkillsBinatang = Binatang['skills']

        // maka kita sudh membuat type baru dengan menampilkan data type Biantang
        let dataKucing: SkillsBinatang = {
            berenang: false,
            lari: true,
            melompat: true
        }
    }

}

{// Gabungan
    {/** Merge
    *  --> jika ada interface yg sama makan akan tergabung jadi satu
    */
        interface Song {
            songName: string;
        }

        interface Song {
            artistName: string;
        }

        const mySong: Song = {
            artistName: 'siapa',
            songName: 'lagu jadul'
        }
    }


    {/** Intersection & Union
    *  --> menggabungkan beberapa type menjadi satu 
    */
        type TypeA = {
            id: number;
            propA: string;
        }

        type TypeB = {
            id: number;
            propB: string;
        }

        // - Intersection ---> semua props yg ada di type wajib di isi
        type IntersectionAB = TypeA & TypeB

        let myData: IntersectionAB = {
            id: 123,
            propA: "stiii",
            propB: "itu"

        }

        // -jika menggunakan union maka props nya menjadi opsional
        type UnionAB = TypeA | TypeB

        let myData2: UnionAB = {
            id: 123,
            propA: "stiii",
            propB: "itu" // menjadi opsional / tidak wajib di isi

        }
    }


    {// extends interface
        interface BasicAddress {
            name: string;
            street: string;
            city: string;

        }

        interface AddressWithUnit extends BasicAddress {
            country: string;
            postalCode?: string;
            unit?: string;
        }

        let resul: AddressWithUnit;
        resul = {
            name: "siapa",
            street: "jalan jalan",
            city: "bandung",
            country: "amerik"
        }
    }
}








