const autoBind = require('auto-bind')

class AlbumLikesHandler {
  constructor(service, albumsService) {
    this._service = service
    this._albumsService = albumsService

    autoBind(this)
  }

  async postAlbumLikeHandler(request, h) {
    const { id: albumId } = request.params
    const { id: credentialId } = request.auth.credentials

    await this._albumsService.getAlbumById(albumId)
    await this._service.addAlbumLikeOrUnlike(credentialId, albumId)

    const response = h.response({
      status: 'success',
      message: 'Berhasil like album',
    })
    response.code(201)
    return response
  }

  async getAlbumLikeHandler(request, h) {
    const { id: albumId } = request.params
    const { likes, isCache } = await this._service.getLikesByIdAlbum(albumId)

    const response = h.response({
      status: 'success',
      data: {
        likes,
      },
    })
    response.code(200)

    if (isCache) response.header('X-Data-Source', 'cache')

    return response
  }
}

module.exports = AlbumLikesHandler
