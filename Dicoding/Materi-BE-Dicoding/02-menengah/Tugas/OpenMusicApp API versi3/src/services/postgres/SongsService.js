const { Pool } = require('pg')
const { nanoid } = require('nanoid')
const InvariantError = require('../../error/InvariantError')
const NotFoundError = require('../../error/NotFoundError')
const { mapDBToModel } = require('../../utils')

class SongsService {
  constructor(cacheService) {
    this._pool = new Pool()
    this._cacheService = cacheService
  }

  async addSongs({ title, year, performer, genre, duration, albumId }) {
    const id = `song-${nanoid(16)}`

    const query = {
      text: 'INSERT INTO songs VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id',
      values: [id, title, year, performer, genre, duration, albumId],
    }

    const result = await this._pool.query(query)

    if (!result.rows[0].id) {
      throw new InvariantError('Gagal menambahkan music baru')
    }

    await this._cacheService.delete('songs')

    return result.rows[0].id
  }

  async getSongs({ title, performer }) {
    try {
      const result = await this._cacheService.get('songs')
      return JSON.parse(result)
    } catch (error) {
      const query = `SELECT id, title, performer FROM songs WHERE LOWER(title) ILIKE('%${title}%') AND LOWER(performer) ILIKE('%${performer}%')`

      const { rows } = await this._pool.query(query)

      await this._cacheService.delete('songs', JSON.stringify(rows))

      return rows
    }
  }

  async getSongById(id) {
    const query = {
      text: 'SELECT * FROM songs WHERE id = $1',
      values: [id],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('Lagu tidak ditemukan')
    }

    await this._cacheService.delete('songs')

    return result.rows.map(mapDBToModel)[0]
  }

  async editSongById(id, { title, year, performer, genre, duration, albumId }) {
    const query = {
      text: 'UPDATE songs SET title = $1, year = $2, performer = $3, genre = $4, duration = $5, album_id = $6 WHERE id = $7 RETURNING id',
      values: [title, year, performer, genre, duration, albumId, id],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('Edit lagu gagal karena ID tidak ditemukan')
    }

    await this._cacheService.delete('songs')
  }

  async deleteSongById(id) {
    const query = {
      text: 'DELETE FROM songs WHERE id = $1 RETURNING id',
      values: [id],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('Gagal hapus lagu karena id tidak ditemukan')
    }

    await this._cacheService.delete('songs')
  }

  async verifyExistingSongById(id) {
    const query = {
      text: 'SELECT * FROM songs WHERE id = $1',
      values: [id],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('Not found music ID!')
    }
  }
}

module.exports = SongsService
