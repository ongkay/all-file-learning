const { Pool } = require('pg')
const { nanoid } = require('nanoid')
const InvariantError = require('../../error/InvariantError')
const NotFoundError = require('../../error/NotFoundError')

class AlbumLikesService {
  constructor(cacheService) {
    this._pool = new Pool()
    this._cacheService = cacheService
  }

  async addAlbumLikeOrUnlike(userId, albumId) {
    const query = {
      text: 'SELECT * FROM user_album_likes WHERE user_id = $1 AND album_id = $2',
      values: [userId, albumId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      await this.goLikeAlbum(userId, albumId)
    } else {
      await this.goUnlikeAlbum(userId, albumId)
    }
    await this._cacheService.delete(`likes:${albumId}`)
  }

  async goLikeAlbum(userId, albumId) {
    const id = `likes-${nanoid(16)}`

    const query = {
      text: 'INSERT INTO user_album_likes VALUES($1, $2, $3) RETURNING id',
      values: [id, userId, albumId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new InvariantError('Like Album Gagal')
    }
  }

  async goUnlikeAlbum(userId, albumId) {
    const query = {
      text: 'DELETE FROM user_album_likes WHERE user_id = $1 AND album_id = $2 RETURNING id',
      values: [userId, albumId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new InvariantError('Unlike Album Gagal')
    }
  }

  async getLikesByIdAlbum(albumId) {
    try {
      const result = await this._cacheService.get(`likes:${albumId}`)

      return {
        likes: JSON.parse(result).length,
        isCache: true,
      }
    } catch (error) {
      const query = {
        text: 'SELECT user_id FROM user_album_likes WHERE album_id = $1',
        values: [albumId],
      }

      const { rows } = await this._pool.query(query)

      if (!rows.length) {
        throw new NotFoundError('Like album tidak ditemukan')
      }

      await this._cacheService.set(`likes:${albumId}`, JSON.stringify(rows))

      return {
        likes: rows.length,
        isCache: false,
      }
    }
  }
}

module.exports = AlbumLikesService
