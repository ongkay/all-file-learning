const Jwt = require('@hapi/jwt')
const InvariantError = require('../error/InvariantError')
const config = require('../utils/config')

const TokenManager = {
  generateAccessToken: async (payload) => Jwt.token.generate(payload, config.jwt.accessTokenKey),
  generateRefreshToken: async (payload) => Jwt.token.generate(payload, config.jwt.refreshTokenKey),
  verifyRefreshToken: async (refreshToken) => {
    try {
      const artifacts = Jwt.token.decode(refreshToken)
      Jwt.token.verifySignature(artifacts, config.jwt.refreshTokenKey)
      const { payload } = artifacts.decoded
      return payload
    } catch (error) {
      throw new InvariantError('Refresh token tidak valid')
    }
  },
}

module.exports = TokenManager
