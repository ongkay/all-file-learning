const { Pool } = require('pg')

class PlaylistsService {
  constructor() {
    this._pool = new Pool()
  }

  async getPlaylists(playlistId) {
    const queryPlaylist = {
      text: 'SELECT playlists.id, playlists.name FROM playlists WHERE id = $1',
      values: [playlistId],
    }

    const querySongs = {
      text: `
        SELECT songs.id, songs.title, songs.performer FROM songs
        LEFT JOIN playlistsongs ON songs.id = playlistsongs.song_id
        WHERE playlistsongs.playlist_id = $1
        `,
      values: [playlistId],
    }

    const { id, name } = (await this._pool.query(queryPlaylist)).rows[0]
    const songs = (await this._pool.query(querySongs)).rows

    return {
      playlist: {
        id,
        name,
        songs,
      },
    }
  }
}

module.exports = PlaylistsService
