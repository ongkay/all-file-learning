const Rocket = require('./Rocket')
const RocketLauncher = require('./RocketLauncher')

describe('A RocketLauncher', () => {
  it('should launch all rockets', () => {
    // Arrange
    const nasaRocket = new Rocket('Nasa')
    const spaceXRocket = new Rocket('SpaceX')
    const rocketLauncher = new RocketLauncher([nasaRocket, spaceXRocket])

    // Action
    rocketLauncher.launchAllRockets()

    // Assert
    expect(nasaRocket.engineStatus).toEqual('active')
    expect(spaceXRocket.engineStatus).toEqual('active')
    expect(rocketLauncher.rockets.length).toEqual(0)
  })

  it('should launch only one rocket by queue', () => {
    // Arrange
    const nasaRocket = new Rocket('Nasa')
    const spaceXRocket = new Rocket('SpaceX')
    const rocketLauncher = new RocketLauncher([nasaRocket, spaceXRocket])

    // Action
    rocketLauncher.launchRocketByQueue()

    // Assert
    expect(nasaRocket.engineStatus).toEqual('active')
    expect(spaceXRocket.engineStatus).toEqual('inactive')
    expect(rocketLauncher.rockets.length).toEqual(1)
  })
})

/** hasil uji
 
 PASS  ./RocketLauncher.test.js
  A RocketLauncher
    ✓ should launch all rockets (8 ms)
    ✓ should launch only one rocket by queue (3 ms)
*/

/**
Arrange : merupakan bagian di mana kita mengumpulkan seluruh kebutuhan untuk melakukan pengujian sesuai skenario. Contohnya membuat objek dependencies, membuat objek konteks yang bakal diuji, ataupun persiapan lainnya. Contoh pada kode di atas adalah kita membuat instance Rocket dan memasukkannya ke instance RocketLauncher agar roket dapat diluncurkan.

Action : merupakan bagian aksi dari sistem yang diuji (System Under Test [SUT]). Biasanya bagian ini hanya terdiri dari satu baris saja. Contohnya adalah memanggil fungsi launchAllRocket.

Assert : merupakan bagian dalam menguji hasil yang diharapkan setelah aksi dilakukan. Contohnya, mengecek apakah jumlah rockets dalam RocketLauncher sudah berkurang dan juga mengecek apakah engineStatus bernilai “active”.
 */
