const AddReplyCommentUseCase = require('../../../../Applications/use_case/AddReplyCommentUseCase')
const DeleteReplyCommentUseCase = require('../../../../Applications/use_case/DeleteReplyCommentUseCase')

class CommentHandler {
  constructor(container) {
    this._container = container

    this.postReplyCommentHandler = this.postReplyCommentHandler.bind(this)
    this.deleteReplyCommentHandler = this.deleteReplyCommentHandler.bind(this)
  }

  async postReplyCommentHandler(request, h) {
    const { commentId, threadId } = request.params
    const { id: credentialId } = request.auth.credentials

    const addReplyCommentUseCase = this._container.getInstance(AddReplyCommentUseCase.name)
    const addedReply = await addReplyCommentUseCase.execute({
      ...request.payload,
      owner: credentialId,
      comment: commentId,
      thread: threadId,
    })

    const response = h.response({
      status: 'success',
      data: {
        addedReply: {
          id: addedReply.id,
          content: addedReply.content,
          owner: addedReply.owner,
        },
      },
    })
    response.code(201)
    return response
  }

  async deleteReplyCommentHandler(request) {
    const { threadId, commentId, replyId } = request.params
    const { id: credentialId } = request.auth.credentials

    const deleteReplyCommentUseCase = this._container.getInstance(DeleteReplyCommentUseCase.name)
    await deleteReplyCommentUseCase.execute({
      id: replyId,
      userId: credentialId,
      threadId,
      commentId,
    })

    return {
      status: 'success',
    }
  }
}

module.exports = CommentHandler
