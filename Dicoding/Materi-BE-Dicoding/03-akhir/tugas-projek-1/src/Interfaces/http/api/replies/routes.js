const routes = (repliesHandler) => [
  {
    method: 'POST',
    path: '/threads/{threadId}/comments/{commentId}/replies',
    handler: repliesHandler.postReplyCommentHandler,
    options: {
      auth: 'forumapi_jwt',
    },
  },
  {
    method: 'DELETE',
    path: '/threads/{threadId}/comments/{commentId}/replies/{replyId}',
    handler: repliesHandler.deleteReplyCommentHandler,
    options: {
      auth: 'forumapi_jwt',
    },
  },
]

module.exports = routes
