const routes = (commentsHandler) => [
  {
    method: 'POST',
    path: '/threads/{threadId}/comments',
    handler: commentsHandler.postCommentHandler,
    options: {
      auth: 'forumapi_jwt',
    },
  },
  {
    method: 'DELETE',
    path: '/threads/{threadId}/comments/{commentId}',
    handler: commentsHandler.deleteCommentHandler,
    options: {
      auth: 'forumapi_jwt',
    },
  },
]

module.exports = routes
