const routes = (threadsHandler) => [
  {
    method: 'POST',
    path: '/threads',
    handler: threadsHandler.postThreadHandler,
    options: {
      auth: 'forumapi_jwt',
    },
  },
  {
    method: 'GET',
    path: '/threads/{id}',
    handler: threadsHandler.getThreadHandler,
  },
]

module.exports = routes
