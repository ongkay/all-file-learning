const NewReply = require('../../Domains/replies/entities/NewReply')

class AddReplyCommentUseCase {
  constructor({ threadRepository, commentRepository, replyRepository }) {
    this._threadRepository = threadRepository
    this._commentRepository = commentRepository
    this._replyRepository = replyRepository
  }

  async execute(useCasePayload) {
    const newReply = new NewReply(useCasePayload)
    await this._commentRepository.checkAvailibilityCommentById(newReply.comment)
    await this._threadRepository.checkAvailibilityThreadById(newReply.thread)
    return this._replyRepository.addReplyComment(newReply)
  }
}

module.exports = AddReplyCommentUseCase
