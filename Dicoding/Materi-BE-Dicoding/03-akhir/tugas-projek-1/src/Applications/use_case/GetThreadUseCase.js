class GetThreadUseCase {
  constructor({ threadRepository, commentRepository, replyRepository }) {
    this._threadRepository = threadRepository
    this._commentRepository = commentRepository
    this._replyRepository = replyRepository
  }

  async execute(useCasePayload) {
    const { id } = useCasePayload
    const thread = await this._threadRepository.getThreadById(id)
    const rawComments = await this._commentRepository.getCommentsByThreadId(id)
    const rawReplies = await this._replyRepository.getReplyByThreadId(id)

    thread.comments = rawComments.map((comment) => {
      const replies = rawReplies
        ?.filter((reply) => reply.comment === comment.id)
        ?.map((reply) => ({
          id: reply.id,
          username: reply.username,
          date: reply.date,
          content: reply.content,
        }))
      return {
        id: comment.id,
        username: comment.username,
        date: comment.date,
        content: comment.content,
        replies,
      }
    })

    return thread
  }
}

module.exports = GetThreadUseCase
