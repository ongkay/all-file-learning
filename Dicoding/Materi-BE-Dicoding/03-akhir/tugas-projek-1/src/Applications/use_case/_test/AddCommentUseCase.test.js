/* eslint-disable implicit-arrow-linebreak */
const NewComment = require('../../../Domains/comments/entities/NewComment')
const AddedComment = require('../../../Domains/comments/entities/AddedComment')
const ThreadRepository = require('../../../Domains/threads/ThreadRepository')
const CommentRepository = require('../../../Domains/comments/CommentRepository')
const AddCommentUseCase = require('../AddCommentUseCase')

describe('AddCommentUseCase', () => {
  it('should orchestrating the add thread comment action correctly', async () => {
    // Arrange
    const useCasePayload = {
      thread: 'thread-abc',
      owner: 'user-abc',
      content: 'Isi konten',
    }

    const expectedAddedComment = new AddedComment({
      ...useCasePayload,
      id: 'comment-abc',
    })

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository()
    const mockCommentRepository = new CommentRepository()

    /** mocking needed function */
    mockThreadRepository.checkAvailibilityThreadById = jest.fn(() => Promise.resolve(useCasePayload.thread))
    mockCommentRepository.addComment = jest.fn(() =>
      Promise.resolve(
        new AddedComment({
          thread: 'thread-abc',
          owner: 'user-abc',
          content: 'Isi konten',
          id: 'comment-abc',
        }),
      ),
    )

    /** creating use case instance */
    const addCommentUseCase = new AddCommentUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
    })

    // Action
    const addedComment = await addCommentUseCase.execute(useCasePayload)

    // Assert
    expect(addedComment).toStrictEqual(expectedAddedComment)
    expect(mockCommentRepository.addComment).toBeCalledWith(new NewComment(useCasePayload))
    expect(mockThreadRepository.checkAvailibilityThreadById).toBeCalledWith(useCasePayload.thread)
  })
})
