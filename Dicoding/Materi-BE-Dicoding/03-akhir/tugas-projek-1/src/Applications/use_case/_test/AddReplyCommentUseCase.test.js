/* eslint-disable implicit-arrow-linebreak */
const NewReply = require('../../../Domains/replies/entities/NewReply')
const AddedReply = require('../../../Domains/replies/entities/AddedReply')
const ThreadRepository = require('../../../Domains/threads/ThreadRepository')
const CommentRepository = require('../../../Domains/comments/CommentRepository')
const ReplyRepository = require('../../../Domains/replies/ReplyRepository')
const AddReplyCommentUseCase = require('../AddReplyCommentUseCase')

describe('AddReplyCommentUseCase', () => {
  it('should orchestrating the add thread comment reply action correctly', async () => {
    // Arrange
    const useCasePayload = {
      thread: 'thread-abc',
      comment: 'comment-abc',
      owner: 'user-abc',
      content: 'Isi konten',
    }
    const expectedAddedReply = new AddedReply({
      ...useCasePayload,
      id: 'reply-abc',
    })

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository()
    const mockCommentRepository = new CommentRepository()
    const mockReplyRepository = new ReplyRepository()

    /** mocking needed function */
    mockThreadRepository.checkAvailibilityThreadById = jest.fn(() => Promise.resolve(useCasePayload.thread))
    mockCommentRepository.checkAvailibilityCommentById = jest.fn(() => Promise.resolve(useCasePayload.comment))
    mockReplyRepository.addReplyComment = jest.fn(() =>
      Promise.resolve(
        new AddedReply({
          thread: 'thread-abc',
          comment: 'comment-abc',
          owner: 'user-abc',
          content: 'Isi konten',
          id: 'reply-abc',
        }),
      ),
    )

    /** creating use case instance */
    const addReplyCommentUseCase = new AddReplyCommentUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
      replyRepository: mockReplyRepository,
    })

    // Action
    const addedReply = await addReplyCommentUseCase.execute(useCasePayload)

    // Assert
    expect(addedReply).toStrictEqual(expectedAddedReply)
    expect(mockReplyRepository.addReplyComment).toBeCalledWith(new NewReply(useCasePayload))
    expect(mockThreadRepository.checkAvailibilityThreadById).toBeCalledWith(useCasePayload.thread)
    expect(mockCommentRepository.checkAvailibilityCommentById).toBeCalledWith(useCasePayload.comment)
  })
})
