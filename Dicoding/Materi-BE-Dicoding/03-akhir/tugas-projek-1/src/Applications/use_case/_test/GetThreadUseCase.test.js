/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable spaced-comment */
const ExistingThread = require('../../../Domains/threads/entities/ExistingThread')
const ExistingComment = require('../../../Domains/comments/entities/ExistingComment')
const ExistingReply = require('../../../Domains/replies/entities/ExistingReply')
const ThreadRepository = require('../../../Domains/threads/ThreadRepository')
const CommentRepository = require('../../../Domains/comments/CommentRepository')
const ReplyRepository = require('../../../Domains/replies/ReplyRepository')
const GetThreadUseCase = require('../GetThreadUseCase')

describe('GetThreadUseCase', () => {
  it('should orchestrating get thread action correctly', async () => {
    // Samples
    const sample = {
      thread: {
        id: 'thread-abc',
        body: 'ini body',
        owner: 'user-abc',
        title: 'Ini Judulnya',
      },
      rawComments: [
        new ExistingComment({
          id: 'comment-abc',
          thread: 'thread-abc',
          username: 'usernamenya',
          owner: 'user-abc',
          date: new Date('2023-01-01'),
          deletedAt: null,
          content: 'kontennnya',
        }),
      ],
      rawReplies: [
        new ExistingReply({
          comment: 'comment-abc',
          id: 'reply-abc',
          username: 'usernamenya',
          owner: 'user-abc',
          date: new Date('2023-01-01'),
          deletedAt: null,
          content: 'kontennnya',
        }),
      ],
    }

    // Arrange
    const useCasePayload = {
      id: 'thread-abc',
    }

    const expectedThreadResult = new ExistingThread({
      ...sample.thread,
      comments: [
        {
          id: 'comment-abc',
          username: 'usernamenya',
          content: 'kontennnya',
          date: new Date('2023-01-01'),
          replies: [
            {
              id: 'reply-abc',
              username: 'usernamenya',
              date: new Date('2023-01-01'),
              content: 'kontennnya',
            },
          ],
        },
      ],
    })

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository()
    const mockCommentRepository = new CommentRepository()
    const mockReplyRepository = new ReplyRepository()

    /** mocking needed function */
    mockThreadRepository.getThreadById = jest.fn().mockImplementation(() => Promise.resolve(new ExistingThread(sample.thread)))
    mockCommentRepository.getCommentsByThreadId = jest.fn(() => Promise.resolve(sample.rawComments))
    mockReplyRepository.getReplyByThreadId = jest.fn(() => Promise.resolve(sample.rawReplies))

    /** creating use case instance */
    const getThreadUseCase = new GetThreadUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
      replyRepository: mockReplyRepository,
    })

    // Action
    const retrievedThread = await getThreadUseCase.execute(useCasePayload)

    // Assert
    expect(retrievedThread).toStrictEqual(expectedThreadResult)
    expect(mockThreadRepository.getThreadById).toBeCalledWith(useCasePayload.id)
    expect(mockCommentRepository.getCommentsByThreadId).toBeCalledWith(useCasePayload.id)
    expect(mockReplyRepository.getReplyByThreadId).toBeCalledWith(useCasePayload.id)
  })
})
