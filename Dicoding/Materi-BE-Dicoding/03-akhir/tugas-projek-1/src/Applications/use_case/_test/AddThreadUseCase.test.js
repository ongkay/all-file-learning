/* eslint-disable implicit-arrow-linebreak */
const NewThread = require('../../../Domains/threads/entities/NewThread')
const AddedThread = require('../../../Domains/threads/entities/AddedThread')
const ThreadRepository = require('../../../Domains/threads/ThreadRepository')
const AddThreadUseCase = require('../AddThreadUseCase')

describe('AddThreadUseCase', () => {
  it('should orchestrating the add thread action correctly', async () => {
    // Arrange
    const useCasePayload = {
      title: 'Ini Judulnya',
      owner: 'user-abc',
      body: 'Isi konten',
    }
    const expectedThreadResult = new AddedThread({
      ...useCasePayload,
      id: 'thread-abc',
    })

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository()

    /** mocking needed function */
    mockThreadRepository.addThread = jest.fn(() =>
      Promise.resolve(
        new AddedThread({
          title: 'Ini Judulnya',
          owner: 'user-abc',
          body: 'Isi konten',
          id: 'thread-abc',
        }),
      ),
    )

    /** creating use case instance */
    const addThreadUseCase = new AddThreadUseCase({
      threadRepository: mockThreadRepository,
    })

    // Action
    const addedThread = await addThreadUseCase.execute(useCasePayload)

    // Assert
    expect(addedThread).toStrictEqual(expectedThreadResult)
    expect(mockThreadRepository.addThread).toBeCalledWith(new NewThread(useCasePayload))
  })
})
