const ThreadRepository = require('../../../Domains/threads/ThreadRepository')
const CommentRepository = require('../../../Domains/comments/CommentRepository')
const DeleteCommentUseCase = require('../DeleteCommentUseCase')

describe('DeleteCommentUseCase', () => {
  it('should orchestrating the delete thread comment action correctly', async () => {
    // Arrange
    const useCasePayload = {
      id: 'comment-abc',
      threadId: 'thread-abc',
      userId: 'user-abc',
    }

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository()
    const mockCommentRepository = new CommentRepository()

    /** mocking needed function */
    mockThreadRepository.checkAvailibilityThreadById = jest.fn(() => Promise.resolve(useCasePayload.threadId))
    mockCommentRepository.checkAvailibilityCommentById = jest.fn(() => Promise.resolve(useCasePayload.id))
    mockCommentRepository.checkCommentOwnership = jest.fn(() => Promise.resolve())
    mockCommentRepository.deleteComment = jest.fn(() => Promise.resolve())

    /** creating use case instance */
    const deleteCommentUseCase = new DeleteCommentUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
    })

    // Action
    await deleteCommentUseCase.execute(useCasePayload)

    // Assert
    expect(mockThreadRepository.checkAvailibilityThreadById).toBeCalledWith(useCasePayload.threadId)
    expect(mockCommentRepository.checkAvailibilityCommentById).toBeCalledWith(useCasePayload.id)
    expect(mockCommentRepository.checkCommentOwnership).toBeCalledWith(useCasePayload.id, useCasePayload.userId)
    expect(mockCommentRepository.deleteComment).toBeCalledWith(useCasePayload.id)
  })
})
