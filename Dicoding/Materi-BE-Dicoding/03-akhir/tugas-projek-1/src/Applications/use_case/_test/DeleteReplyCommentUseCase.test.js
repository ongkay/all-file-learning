const ThreadRepository = require('../../../Domains/threads/ThreadRepository')
const CommentRepository = require('../../../Domains/comments/CommentRepository')
const ReplyRepository = require('../../../Domains/replies/ReplyRepository')
const DeleteReplyCommentUseCase = require('../DeleteReplyCommentUseCase')

describe('DeleteReplyCommentUseCase', () => {
  it('should orchestrating the delete thread comment reply action correctly', async () => {
    // Arrange
    const useCasePayload = {
      id: 'reply-abc',
      commentId: 'comment-abc',
      threadId: 'thread-abc',
      userId: 'user-abc',
    }

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository()
    const mockCommentRepository = new CommentRepository()
    const mockReplyRepository = new ReplyRepository()

    /** mocking needed function */
    mockThreadRepository.checkAvailibilityThreadById = jest.fn(() => Promise.resolve(useCasePayload.threadId))
    mockCommentRepository.checkAvailibilityCommentById = jest.fn(() => Promise.resolve(useCasePayload.commentId))
    mockReplyRepository.checkAvailibilityReplyById = jest.fn(() => Promise.resolve(useCasePayload.id))
    mockReplyRepository.checkReplyOwnership = jest.fn(() => Promise.resolve())
    mockReplyRepository.deleteReplyComment = jest.fn(() => Promise.resolve())

    /** creating use case instance */
    const deleteReplyCommentUseCase = new DeleteReplyCommentUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
      replyRepository: mockReplyRepository,
    })

    // Action
    await deleteReplyCommentUseCase.execute(useCasePayload)

    // Assert
    expect(mockThreadRepository.checkAvailibilityThreadById).toBeCalledWith(useCasePayload.threadId)
    expect(mockCommentRepository.checkAvailibilityCommentById).toBeCalledWith(useCasePayload.commentId)
    expect(mockReplyRepository.checkAvailibilityReplyById).toBeCalledWith(useCasePayload.id)
    expect(mockReplyRepository.checkReplyOwnership).toBeCalledWith(useCasePayload.id, useCasePayload.userId)
    expect(mockReplyRepository.deleteReplyComment).toBeCalledWith(useCasePayload.id)
  })
})
