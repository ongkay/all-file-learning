const AuthorizationError = require('../../Commons/exceptions/AuthorizationError')
const NotFoundError = require('../../Commons/exceptions/NotFoundError')
const CommentRepository = require('../../Domains/comments/CommentRepository')
const AddedComment = require('../../Domains/comments/entities/AddedComment')
const ExistingComment = require('../../Domains/comments/entities/ExistingComment')

class CommentRepositoryPostgres extends CommentRepository {
  constructor(pool, idGenerator) {
    super()
    this._pool = pool
    this._idGenerator = idGenerator
  }

  async addComment(newComment) {
    const { thread, owner, content } = newComment
    const id = `comment-${this._idGenerator()}`

    const query = {
      text: 'INSERT INTO comments_thread VALUES($1, $2, $3, $4) RETURNING id, thread, owner, content',
      values: [id, thread, owner, content],
    }

    await this._pool.query(query)

    return new AddedComment({ ...newComment, id })
  }

  async deleteComment(commentId) {
    const deletedAt = new Date().toISOString()
    const query = {
      text: 'UPDATE comments_thread SET "deletedAt" = $1 WHERE id = $2 ',
      values: [deletedAt, commentId],
    }

    await this._pool.query(query)
  }

  async checkAvailibilityCommentById(threadCommentId) {
    const query = {
      text: 'SELECT * FROM comments_thread WHERE id = $1',
      values: [threadCommentId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('komentar tidak ditemukan')
    }
  }

  async checkCommentOwnership(threadCommentId, userId) {
    const query = {
      text: 'SELECT * FROM comments_thread WHERE id = $1 AND owner = $2',
      values: [threadCommentId, userId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new AuthorizationError('ini bukan pemilik komentar ini')
    }
  }

  async getCommentsByThreadId(threadId) {
    const query = {
      text: `
        SELECT comments_thread.id, comments_thread.thread, users.username, comments_thread.owner, comments_thread."createdAt" AS "date", comments_thread.content, comments_thread."deletedAt"
        FROM comments_thread 
          LEFT JOIN users ON users.id = comments_thread.owner 
          WHERE comments_thread.thread = $1 
          ORDER BY comments_thread."createdAt" ASC
      `,
      values: [threadId],
    }

    const result = await this._pool.query(query)

    return result?.rows?.map((e) => new ExistingComment(e))
  }
}

module.exports = CommentRepositoryPostgres
