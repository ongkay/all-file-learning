const { nanoid } = require('nanoid')
const UsersTableTestHelper = require('../../../../tests/UsersTableTestHelper')
const ThreadTableTestHelper = require('../../../../tests/ThreadTableTestHelper')
const ThreadCommentTableTestHelper = require('../../../../tests/ThreadCommentTableTestHelper')
const ThreadCommentReplyTableTestHelper = require('../../../../tests/ThreadCommentReplyTableTestHelper')
const AuthorizationError = require('../../../Commons/exceptions/AuthorizationError')
const NotFoundError = require('../../../Commons/exceptions/NotFoundError')
const NewReply = require('../../../Domains/replies/entities/NewReply')
const pool = require('../../database/postgres/pool')
const ReplyRepositoryPostgres = require('../ReplyRepositoryPostgres')
const ExistingReply = require('../../../Domains/replies/entities/ExistingReply')
const AddedReply = require('../../../Domains/replies/entities/AddedReply')

describe('ReplyRepositoryPostgres', () => {
  const user = {
    id: 'user-abcdef',
    username: 'ongkay',
    password: 'secret',
    fullname: 'Ongkay S.',
  }

  const sampleThread = {
    id: 'thread-abcdef',
    title: 'Ini Adalah Judul',
    body: 'Ini adalah body Thread',
    owner: user.id,
  }

  const sampleThreadComment = {
    id: 'comment-abcdef',
    thread: sampleThread.id,
    owner: user.id,
    content: 'Ini komentar',
  }

  beforeAll(async () => {
    await ThreadCommentReplyTableTestHelper.cleanTable()
    await ThreadCommentTableTestHelper.cleanTable()
    await ThreadTableTestHelper.cleanTable()
    await UsersTableTestHelper.cleanTable()
    await UsersTableTestHelper.addUser(user)
    await ThreadTableTestHelper.addThread(sampleThread)
    await ThreadCommentTableTestHelper.addThreadComment(sampleThreadComment)
  })

  afterAll(async () => {
    await ThreadCommentReplyTableTestHelper.cleanTable()
    await ThreadCommentTableTestHelper.cleanTable()
    await ThreadTableTestHelper.cleanTable()
    await UsersTableTestHelper.cleanTable()
    await pool.end()
  })

  const sampleThreadCommentReply = {
    owner: user.id,
    content: 'Ini balasan komentar',
  }

  describe('addReplyComment function', () => {
    it('should add one thread comment reply to database', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)

      // Action
      const addedReply = await replyRepository.addReplyComment(
        new NewReply({
          ...sampleThreadCommentReply,
          comment: sampleThreadComment.id,
          thread: sampleThread.id,
        }),
      )
      expect(addedReply).toBeInstanceOf(AddedReply)
      sampleThreadCommentReply.id = addedReply.id

      // Assert
      const threadCommentReply = await ThreadCommentReplyTableTestHelper.findOneById(addedReply.id)
      expect(threadCommentReply).toHaveProperty('id')
      expect(threadCommentReply.content).toStrictEqual(sampleThreadCommentReply.content)
      expect(threadCommentReply.comment).toStrictEqual(sampleThreadComment.id)
      expect(threadCommentReply.owner).toStrictEqual(user.id)
    })
  })

  describe('checkAvailibilityReplyById function', () => {
    it('should return nothing if found', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)
      // const spy = jest.spyOn(replyRepository, 'checkAvailibilityReplyById')

      // Action & Assert
      // await replyRepository.checkAvailibilityReplyById(sampleThreadCommentReply.id)
      // expect(spy).toHaveBeenCalledTimes(1)
      await expect(replyRepository.checkAvailibilityReplyById(sampleThreadCommentReply.id)).resolves.not.toThrowError()
    })
    it('should throw error if not found', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)

      // Action & Assert
      expect(async () => {
        await replyRepository.checkAvailibilityReplyById('reply-abc')
      }).rejects.toThrow(NotFoundError)
    })
  })

  describe('checkReplyOwnership function', () => {
    it('should be okay if owned', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)
      const spy = jest.spyOn(replyRepository, 'checkReplyOwnership')

      // Action & Assert
      await replyRepository.checkReplyOwnership(sampleThreadCommentReply.id, user.id)
      expect(spy).toHaveBeenCalledTimes(1)
    })
    it('should throw error if not owned', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)

      // Action & Assert
      expect(async () => {
        await replyRepository.checkReplyOwnership(sampleThreadCommentReply.id, 'user-abc')
      }).rejects.toThrow(AuthorizationError)
    })
  })

  describe('getReplyByThreadId function', () => {
    it('should get thread comment replies by thread id from database', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)

      // Action & Assert
      const threadCommentReplies = await replyRepository.getReplyByThreadId(sampleThread.id)
      expect(threadCommentReplies).toHaveLength(1)
      expect(threadCommentReplies[0]).toBeInstanceOf(ExistingReply)
      expect(threadCommentReplies[0].id).toStrictEqual(sampleThreadCommentReply.id)
      expect(threadCommentReplies[0].owner).toStrictEqual(sampleThreadCommentReply.owner)
      expect(threadCommentReplies[0].comment).toStrictEqual(sampleThreadComment.id)
      expect(threadCommentReplies[0].username).toStrictEqual(user.username)
      expect(threadCommentReplies[0].content).toStrictEqual(sampleThreadCommentReply.content)
      expect(threadCommentReplies[0]).toHaveProperty('date')
    })
  })

  describe('deleteReplyComment function', () => {
    it('should delete one thread comment reply from database', async () => {
      // Arrange
      const replyRepository = new ReplyRepositoryPostgres(pool, nanoid)

      // Action
      await replyRepository.deleteReplyComment(sampleThreadCommentReply.id)

      // Assert
      const threadCommentReply = await ThreadCommentReplyTableTestHelper.findOneById(sampleThreadCommentReply.id)
      expect(threadCommentReply.deletedAt).not.toBeNull()
    })
  })
})
