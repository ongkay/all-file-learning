const AuthorizationError = require('../../Commons/exceptions/AuthorizationError')
const NotFoundError = require('../../Commons/exceptions/NotFoundError')
const AddedReply = require('../../Domains/replies/entities/AddedReply')
const ExistingReply = require('../../Domains/replies/entities/ExistingReply')
const ReplyRepository = require('../../Domains/replies/ReplyRepository')

class ReplyRepositoryPostgres extends ReplyRepository {
  constructor(pool, idGenerator) {
    super()
    this._pool = pool
    this._idGenerator = idGenerator
  }

  async addReplyComment(newReply) {
    const { comment, owner, content } = newReply
    const id = `reply-${this._idGenerator()}`

    const query = {
      text: 'INSERT INTO replies_comment VALUES($1, $2, $3, $4) RETURNING id, comment, owner, content',
      values: [id, comment, owner, content],
    }

    await this._pool.query(query)

    return new AddedReply({ ...newReply, id })
  }

  async deleteReplyComment(replyId) {
    const deletedAt = new Date().toISOString()
    const query = {
      text: 'UPDATE replies_comment SET "deletedAt" = $1 WHERE id = $2 ',
      values: [deletedAt, replyId],
    }

    await this._pool.query(query)
  }

  async checkAvailibilityReplyById(threadCommentReplyId) {
    const query = {
      text: 'SELECT * FROM replies_comment WHERE id = $1',
      values: [threadCommentReplyId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new NotFoundError('balasan komentar ini tidak ditemukan')
    }
  }

  async checkReplyOwnership(threadCommentReplyId, userId) {
    const query = {
      text: 'SELECT * FROM replies_comment WHERE id = $1 AND owner = $2',
      values: [threadCommentReplyId, userId],
    }

    const result = await this._pool.query(query)

    if (!result.rowCount) {
      throw new AuthorizationError('bukan pemilik balasan komentar ini')
    }
  }

  async getReplyByThreadId(threadId) {
    const query = {
      text: `
        SELECT replies_comment.comment AS comment ,replies_comment.id, replies_comment.content, replies_comment."createdAt" AS "date", users.username, replies_comment."deletedAt", replies_comment.owner 
        FROM replies_comment 
          LEFT JOIN comments_thread ON replies_comment.comment = comments_thread.id
          LEFT JOIN users ON users.id = replies_comment.owner 
          WHERE comments_thread.thread = $1 
          ORDER BY replies_comment."createdAt" ASC
      `,
      values: [threadId],
    }

    const result = await this._pool.query(query)

    return result?.rows?.map((r) => new ExistingReply(r))
  }
}

module.exports = ReplyRepositoryPostgres
