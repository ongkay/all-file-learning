const ExistingReply = require('../ExistingReply')

describe('ExistingReply entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      comment: 'comment-abc',
    }

    // Action & Assert
    expect(() => new ExistingReply(payload)).toThrowError('EXISTING_THREAD_COMMENT_REPLY.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 'reply-abc',
      username: 'username',
      comment: 'comment-abc',
      owner: 123456,
      content: 'Kontennya',
      date: new Date(),
    }

    // Action & Assert
    expect(() => new ExistingReply(payload)).toThrowError('EXISTING_THREAD_COMMENT_REPLY.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create ExistingReply entities correctly', () => {
    // Arrange
    const payload = {
      id: 'reply-abc',
      username: 'username',
      comment: 'comment-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
      date: new Date(),
      deletedAt: null,
    }

    // Action
    const existingThreadCommentReply = new ExistingReply(payload)

    // Assert
    expect(existingThreadCommentReply).toBeInstanceOf(ExistingReply)
    expect(existingThreadCommentReply.id).toEqual(payload.id)
    expect(existingThreadCommentReply.username).toEqual(payload.username)
    expect(existingThreadCommentReply.date).toEqual(payload.date)
    expect(existingThreadCommentReply.comment).toEqual(payload.comment)
    expect(existingThreadCommentReply.owner).toEqual(payload.owner)
    expect(existingThreadCommentReply.content).toEqual(payload.content)
  })

  it('should create ExistingReply entities when soft deleted correctly', () => {
    // Arrange
    const payload = {
      id: 'thread-abc',
      username: 'username',
      comment: 'comment-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
      deletedAt: new Date(),
      date: new Date(),
    }

    // Action
    const existingThreadCommentReply = new ExistingReply(payload)

    // Assert
    expect(existingThreadCommentReply).toBeInstanceOf(ExistingReply)
    expect(existingThreadCommentReply.id).toEqual(payload.id)
    expect(existingThreadCommentReply.username).toEqual(payload.username)
    expect(existingThreadCommentReply.date).toEqual(payload.date)
    expect(existingThreadCommentReply.comment).toEqual(payload.comment)
    expect(existingThreadCommentReply.owner).toEqual(payload.owner)
    expect(existingThreadCommentReply.content).toEqual('**balasan telah dihapus**')
  })
})
