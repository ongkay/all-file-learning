const NewReply = require('../NewReply')

describe('NewReply entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      thread: 'thread-abc',
    }

    // Action & Assert
    expect(() => new NewReply(payload)).toThrowError('NEW_REPLY_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      thread: 'thread-abc',
      comment: 'comment-abc',
      owner: 123456,
      content: 'Kontennya',
    }

    // Action & Assert
    expect(() => new NewReply(payload)).toThrowError('NEW_REPLY_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create NewReply entities correctly', () => {
    // Arrange
    const payload = {
      thread: 'thread-abc',
      comment: 'comment-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
    }

    // Action
    const newReply = new NewReply(payload)

    // Assert
    expect(newReply).toBeInstanceOf(NewReply)
    expect(newReply.thread).toEqual(payload.thread)
    expect(newReply.comment).toEqual(payload.comment)
    expect(newReply.owner).toEqual(payload.owner)
    expect(newReply.content).toEqual(payload.content)
  })
})
