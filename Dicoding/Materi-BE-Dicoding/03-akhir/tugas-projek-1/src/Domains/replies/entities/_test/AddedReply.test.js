const AddedReply = require('../AddedReply')

describe('AddedReply entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      thread: 'thread-abc',
    }

    // Action & Assert
    expect(() => new AddedReply(payload)).toThrowError('ADDED_THREAD_COMMENT_REPLY.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 'reply-abc',
      comment: 'comment-abc',
      owner: 123456,
      content: 'Kontennya',
    }

    // Action & Assert
    expect(() => new AddedReply(payload)).toThrowError('ADDED_THREAD_COMMENT_REPLY.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create AddedReply entities correctly', () => {
    // Arrange
    const payload = {
      id: 'reply-abc',
      comment: 'comment-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
    }

    // Action
    const addedReply = new AddedReply(payload)

    // Assert
    expect(addedReply).toBeInstanceOf(AddedReply)
    expect(addedReply.id).toEqual(payload.id)
    expect(addedReply.comment).toEqual(payload.comment)
    expect(addedReply.owner).toEqual(payload.owner)
    expect(addedReply.content).toEqual(payload.content)
  })
})
