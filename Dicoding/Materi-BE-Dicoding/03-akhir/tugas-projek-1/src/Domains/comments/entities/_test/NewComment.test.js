const NewComment = require('../NewComment')

describe('NewComment entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      thread: 'thread-h_W1Plfpj0TY7wyT2PUPX',
    }

    // Action & Assert
    expect(() => new NewComment(payload)).toThrowError('NEW_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      thread: 'thread-h_W1Plfpj0TY7wyT2PUPX',
      owner: 312312,
      content: 'Isi Badan',
    }

    // Action & Assert
    expect(() => new NewComment(payload)).toThrowError('NEW_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create NewComment entities correctly', () => {
    // Arrange
    const payload = {
      thread: 'thread-h_W1Plfpj0TY7wyT2PUPX',
      owner: 'user-DWrT3pXe1hccYkV1eIAxSx',
      content: 'Isi Badan',
    }

    // Action
    const newComment = new NewComment(payload)

    // Assert
    expect(newComment).toBeInstanceOf(NewComment)
    expect(newComment.thread).toEqual(payload.thread)
    expect(newComment.owner).toEqual(payload.owner)
    expect(newComment.content).toEqual(payload.content)
  })
})
