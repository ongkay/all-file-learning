const AddedComment = require('../AddedComment')

describe('AddedComment entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      thread: 'thread-abc',
    }

    // Action & Assert
    expect(() => new AddedComment(payload)).toThrowError('ADDED_THREAD_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 'comment-abc',
      thread: 'thread-abc',
      owner: 123456,
      content: 'Kontennya',
    }

    // Action & Assert
    expect(() => new AddedComment(payload)).toThrowError('ADDED_THREAD_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create AddedComment entities correctly', () => {
    // Arrange
    const payload = {
      id: 'comment-abc',
      thread: 'thread-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
    }

    // Action
    const addedComment = new AddedComment(payload)

    // Assert
    expect(addedComment).toBeInstanceOf(AddedComment)
    expect(addedComment.id).toEqual(payload.id)
    expect(addedComment.thread).toEqual(payload.thread)
    expect(addedComment.owner).toEqual(payload.owner)
    expect(addedComment.content).toEqual(payload.content)
  })
})
