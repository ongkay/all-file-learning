const ExistingComment = require('../ExistingComment')

describe('ExistingComment entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      thread: 'thread-abc',
    }

    // Action & Assert
    expect(() => new ExistingComment(payload)).toThrowError('EXISTING_THREAD_COMMENT.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      id: 'comment-abc',
      username: 'user-abc',
      date: new Date(),
      thread: 'thread-abc',
      owner: 123456,
      content: 'Kontennya',
    }

    // Action & Assert
    expect(() => new ExistingComment(payload)).toThrowError('EXISTING_THREAD_COMMENT.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create ExistingComment entities correctly', () => {
    // Arrange
    const payload = {
      id: 'comment-abc',
      username: 'user-abc',
      date: new Date(),
      thread: 'thread-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
      deletedAt: null,
    }

    // Action
    const existingComment = new ExistingComment(payload)

    // Assert
    expect(existingComment).toBeInstanceOf(ExistingComment)
    expect(existingComment.id).toEqual(payload.id)
    expect(existingComment.username).toEqual(payload.username)
    expect(existingComment.date).toEqual(payload.date)
    expect(existingComment.thread).toEqual(payload.thread)
    expect(existingComment.owner).toEqual(payload.owner)
    expect(existingComment.content).toEqual(payload.content)
    expect(existingComment.replies).toHaveLength(0)
  })

  it('should create ExistingComment entities that is deleted correctly', () => {
    // Arrange
    const payload = {
      id: 'comment-abc',
      username: 'user-abc',
      date: new Date(),
      thread: 'thread-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
      deletedAt: new Date(),
    }

    // Action
    const existingComment = new ExistingComment(payload)

    // Assert
    expect(existingComment).toBeInstanceOf(ExistingComment)
    expect(existingComment.id).toEqual(payload.id)
    expect(existingComment.username).toEqual(payload.username)
    expect(existingComment.date).toEqual(payload.date)
    expect(existingComment.thread).toEqual(payload.thread)
    expect(existingComment.owner).toEqual(payload.owner)
    expect(existingComment.content).toEqual('**komentar telah dihapus**')
    expect(existingComment.replies).toHaveLength(0)
  })

  it('should create ExistingComment entities with replies data correctly', () => {
    // Arrange
    const payload = {
      id: 'comment-abc',
      username: 'user-abc',
      date: new Date(),
      thread: 'thread-abc',
      owner: 'user-abcd',
      content: 'Kontennya',
      replies: [],
      deletedAt: null,
    }

    // Action
    const existingComment = new ExistingComment(payload)

    // Assert
    expect(existingComment).toBeInstanceOf(ExistingComment)
    expect(existingComment.id).toEqual(payload.id)
    expect(existingComment.username).toEqual(payload.username)
    expect(existingComment.date).toEqual(payload.date)
    expect(existingComment.thread).toEqual(payload.thread)
    expect(existingComment.owner).toEqual(payload.owner)
    expect(existingComment.content).toEqual(payload.content)
    expect(existingComment.replies).toHaveLength(0)
  })
})
