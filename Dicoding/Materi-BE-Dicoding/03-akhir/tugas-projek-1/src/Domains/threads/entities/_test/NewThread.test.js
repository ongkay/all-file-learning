const NewThread = require('../NewThread')

describe('NewThread entities', () => {
  it('should throw error when payload not contain needed property', () => {
    // Arrange
    const payload = {
      title: 'Ini Judulnya',
    }

    // Action & Assert
    expect(() => new NewThread(payload)).toThrowError('NEW_THREAD.NOT_CONTAIN_NEEDED_PROPERTY')
  })

  it('should throw error when payload not meet data type specification', () => {
    // Arrange
    const payload = {
      title: 'Ini Judulnya',
      owner: 123456,
      body: 'Kontennya',
    }

    // Action & Assert
    expect(() => new NewThread(payload)).toThrowError('NEW_THREAD.NOT_MEET_DATA_TYPE_SPECIFICATION')
  })

  it('should create NewThread entities correctly', () => {
    // Arrange
    const payload = {
      title: 'Ini Judulnya',
      owner: 'user-abcd',
      body: 'Kontennya',
    }

    // Action
    const newThread = new NewThread(payload)

    // Assert
    expect(newThread).toBeInstanceOf(NewThread)
    expect(newThread.title).toEqual(payload.title)
    expect(newThread.owner).toEqual(payload.owner)
    expect(newThread.body).toEqual(payload.body)
  })
})
