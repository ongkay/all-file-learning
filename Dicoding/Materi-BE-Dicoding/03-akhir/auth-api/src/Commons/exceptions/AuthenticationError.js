const ClientError = require('./ClientError')

class AuthenticationError extends ClientError {
  constructor(message) {
    super(message, 401)
    this.name = 'AuthenticationError'
  }
}

module.exports = AuthenticationError

// AuthenticationError (extends dari ClientError) : Custom error yang mengindikasikan eror karena masalah autentikasi. Contohnya password yang diberikan salah dan refresh token yang diberikan tidak valid.
