class ClientError extends Error {
  constructor(message, statusCode = 400) {
    super(message)

    if (this.constructor.name === 'ClientError') {
      throw new Error('cannot instantiate abstract class')
    }

    this.statusCode = statusCode
    this.name = 'ClientError'
  }
}

module.exports = ClientError

//ClientError (extends dari Error) : Custom error yang mengindikasikan eror karena masalah yang terjadi pada client. ClientError ini bersifat abstrak karena client error bisa lebih spesifik. Sebaiknya Anda tidak membangkitkan error menggunakan class ini secara langsung, gunakan turunannya saja.
