const AddUserUseCase = require('../../../../Applications/use_case/AddUserUseCase')

class UsersHandler {
  constructor(container) {
    this._container = container
    this.postUserHandler = this.postUserHandler.bind(this)
  }

  async postUserHandler(request, h) {
    const addUserUseCase = this._container.getInstance(AddUserUseCase.name)
    const addedUser = await addUserUseCase.execute(request.payload)

    const response = h.response({
      status: 'success',
      data: {
        addedUser,
      },
    })
    response.code(201)
    return response
  }
}

module.exports = UsersHandler
/** Fungsi handler (postUserHandler) dalam kode tersebut tidak ada lagi logika yang dituliskan selain untuk merespons permintaan karena
 * semua logic akan ditangani oleh use case.
 * Handler memanggil fungsi execute dari use case dan memberikan request.payload sebagai payload use case.
 * Handler memiliki akses terhadap instance use case melalui container yang akan dikirim oleh HTTP server melalui plugin options.
 * Perhatikan juga bahwa untuk mendapatkan instance dari container, kita menggunakan fungsi getInstance(key).
 */
