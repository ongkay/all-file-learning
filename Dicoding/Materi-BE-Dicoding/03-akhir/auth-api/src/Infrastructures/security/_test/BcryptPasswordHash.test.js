const bcrypt = require('bcrypt')
const BcryptPasswordHash = require('../BcryptPasswordHash')

describe('BcryptPasswordHash', () => {
  describe('hash function', () => {
    it('should encrypt password correctly', async () => {
      // Arrange
      const spyHash = jest.spyOn(bcrypt, 'hash')
      const bcryptPasswordHash = new BcryptPasswordHash(bcrypt)

      // Action
      const encryptedPassword = await bcryptPasswordHash.hash('plain_password')

      // Assert
      expect(typeof encryptedPassword).toEqual('string')
      expect(encryptedPassword).not.toEqual('plain_password')
      expect(spyHash).toBeCalledWith('plain_password', 10) // 10 adalah nilai saltRound default untuk BcryptPasswordHash
    })
  })
})

/** hash function should encrypt password correctly
 * Skenario ini menguji kebenaran dari fungsi hash dalam mengenkripsi password menggunakan bcrypt.
 * Di sini, kita menggunakan teknik spy untuk melihat apakah fungsi hash dari bcrypt dipanggil dan diperlakukan dengan benar.
 * Di sini juga kita memastikan nilai “plain_password” sudah terenkripsi.
 */
