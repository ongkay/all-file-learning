const UsersTableTestHelper = require('../../../../tests/UsersTableTestHelper')
const InvariantError = require('../../../Commons/exceptions/InvariantError')
const RegisterUser = require('../../../Domains/users/entities/RegisterUser')
const RegisteredUser = require('../../../Domains/users/entities/RegisteredUser')
const pool = require('../../database/postgres/pool')
const UserRepositoryPostgres = require('../UserRepositoryPostgres')

describe('UserRepositoryPostgres', () => {
  afterEach(async () => {
    await UsersTableTestHelper.cleanTable()
  })

  afterAll(async () => {
    await pool.end()
  })

  describe('verifyAvailableUsername function', () => {
    /** kita akan menguji keadaan fungsi ketika username tidak tersedia atau sudah terdaftar di database.
     * Pengujian ini memastikan verifyAvailableUsername membangkitkan InvariantError. */
    it('should throw InvariantError when username not available', async () => {
      // Arrange
      await UsersTableTestHelper.addUser({ username: 'dicoding' }) // memasukan user baru dengan username dicoding
      const userRepositoryPostgres = new UserRepositoryPostgres(pool, {})

      // Action & Assert
      await expect(
        userRepositoryPostgres.verifyAvailableUsername('dicoding'),
      ).rejects.toThrowError(InvariantError)
    })

    /** di sini kita akan menguji keadaan di mana username baru dapat digunakan atau belum ada yang menggunakannya di database.
     * Table helper tidak digunakan pada pengujian ini karena tidak dibutuhkan. */
    it('should not throw InvariantError when username available', async () => {
      // Arrange
      const userRepositoryPostgres = new UserRepositoryPostgres(pool, {})

      // Action & Assert
      await expect(
        userRepositoryPostgres.verifyAvailableUsername('dicoding'),
      ).resolves.not.toThrowError(InvariantError)
    })
  })

  describe('addUser function', () => {
    /** untuk menguji apakah fungsi addUser mampu menyimpan user baru pada database dengan benar.
     * Kita menggunakan UsersTableTestHelper.findUsersById untuk menguji apakah user dimasukkan ke database. */
    it('should persist register user', async () => {
      // Arrange
      const registerUser = new RegisterUser({
        username: 'dicoding',
        password: 'secret_password',
        fullname: 'Dicoding Indonesia',
      })
      const fakeIdGenerator = () => '123' // generate id seperti nanoid
      const userRepositoryPostgres = new UserRepositoryPostgres(pool, fakeIdGenerator)

      // Action
      await userRepositoryPostgres.addUser(registerUser)

      // Assert
      const users = await UsersTableTestHelper.findUsersById('user-123')
      expect(users).toHaveLength(1)
    })

    /** Selain dapat menyimpan data di database, fungsi add user juga harus mengembalikan nilai id, username, -
     *  dan fullname dari user yang baru saja dimasukkan ke database (RegisteredUser).
     * Di sini kita menggunakan table helper untuk melihat apakah data user benar-benar dimasukkan ke dalam database */
    it('should return registered user correctly', async () => {
      // Arrange
      const registerUser = new RegisterUser({
        username: 'dicoding',
        password: 'secret_password',
        fullname: 'Dicoding Indonesia',
      })
      const fakeIdGenerator = () => '123' // stub!
      const userRepositoryPostgres = new UserRepositoryPostgres(pool, fakeIdGenerator)

      // Action
      const registeredUser = await userRepositoryPostgres.addUser(registerUser)

      // Assert
      expect(registeredUser).toStrictEqual(
        new RegisteredUser({
          id: 'user-123',
          username: 'dicoding',
          fullname: 'Dicoding Indonesia',
        }),
      )
    })
  })
})

/** Pada skenario di atas juga kita menggunakan describe untuk membagi skenario pengujian berdasarkan konteksnya.
 * Kami yakin Anda sudah mengetahui fungsi dari describe ini. Hal baru yang ada di sana adalah penggunaan fungsi afterEach dan afterAll.
 *
 * Fungsi afterEach dan afterAll merupakan fungsi yang digunakan untuk menampung sekumpulan kode yang dijalankan setelah melakukan pengujian.
 * Bedanya fungsi afterEach dieksekusi setiap kali fungsi it selesai dijalankan, sedangkan afterAll dieksekusi setelah seluruh fungsi it selesai dijalankan.
 * Biasanya kode yang dituliskan di fungsi ini adalah kode cleaning atau teardown.
 * Untuk mengenal lebih dalam kedua fungsi ini disarankan untuk membaca dokumentasi yang diberikan Jest mengenai Setup and Teardown.
 * --> https://jestjs.io/docs/setup-teardown
 * */
